package duan.test.lavendimia.enums;

public enum FragmentAction {
    SIGN_UP_FAILED, SIGN_UP_SUCCESS, CHANGE_LANGUAJE, LOST_PASSWORD, LOG_IN, LOGIN_SUCCESS, LOGIN_FAILED
}
