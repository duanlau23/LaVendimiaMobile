package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.custom_views.custom_recyclerview.MonthlyPaymentAdapter;
import duan.test.lavendimia.custom_views.custom_recyclerview.SaleItemAdapter;
import duan.test.lavendimia.custom_views.multiple_picker.MultiplePicker;
import duan.test.lavendimia.enums.PickerType;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.listeners.ObservableItemListener;
import duan.test.lavendimia.listeners.PickerListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.MonthlyPayment;
import duan.test.lavendimia.objects.MultipleObject;
import duan.test.lavendimia.objects.Sale;
import duan.test.lavendimia.objects.observables.ItemObserver;
import duan.test.lavendimia.objects.observables.ObservableItem;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;


public class AddSaleFragment extends DialogFragment
        implements View.OnClickListener,
        ObservableItemListener {

    View v;
    Sale sale;
    TextView folioView;
    TextView rfcView;
    EditText clientView;
    EditText itemView;
    TextView hitchView;
    TextView hitchBonificationView;
    TextView totalView;
    View saveButton;
    View addButon;
    RecyclerView itemsRecyclerView;
    SaleItemAdapter saleItemAdapter;
    ObservableItem observableItem;
    ItemObserver itemObserver;
    List<MonthlyPayment> payments;
    RecyclerView monthlyPaymentsRecyclerView;
    MonthlyPaymentAdapter monthlyPaymentAdapter;
    private boolean isInEditMode;
    private boolean itemIsSaved;
    ProgressDialog progressDialog;
    String folio;
    Client selectedClient;
    Item selectedItem;
    float hitch = 0;
    float hitchBonitication = 0;
    float total = 0;
    float countedPrice = 0;
    SharedPreferences prefs;
    boolean clientHasError;
    boolean itemsHasError;
    boolean paymentsHasError;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() == null)
            return;

        sale = getArguments().getParcelable("sale");
        isInEditMode = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar actionBar = (Toolbar) v.findViewById(R.id.settings_toolbar);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
        if (actionBar != null) {
            final AddSaleFragment window = this;
            actionBar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                }
            });
        }

        prefs = getContext().getSharedPreferences(Utils.APLICATION_ID, Context.MODE_PRIVATE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

    }

    public AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppThemeDark);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                if(itemIsSaved)
                    return true;

                if ((i ==  KeyEvent.KEYCODE_BACK) && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                    return false;
                }
                else
                    return false;
            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_create_sale, null);
        setupViews();
        setupRecyclerView();
        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        clientView = (EditText) v.findViewById(R.id.client_view);
        rfcView = (TextView) v.findViewById(R.id.client_rfc);
        itemView = (EditText) v.findViewById(R.id.item_view);
        addButon = v.findViewById(R.id.add_button);
        saveButton = v.findViewById(R.id.save_button);
        folioView = (TextView) v.findViewById(R.id.folio_view);
        itemsRecyclerView = (RecyclerView) v.findViewById(R.id.items_list);
        hitchView = (TextView) v.findViewById(R.id.hitch_total_view);
        hitchBonificationView = (TextView) v.findViewById(R.id.hitch_bonification_total_view);
        totalView = (TextView) v.findViewById(R.id.total_view);
        monthlyPaymentsRecyclerView = (RecyclerView) v.findViewById(R.id.monthly_payment_list);

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.saving_title_text));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);

        if(sale != null){
            folio = sale.getFolio();
        }else{
            folio = "0001";
            if(Utils.OBSERVABLE_SALE_DATA != null){
                if(Utils.OBSERVABLE_SALE_DATA.getData().size() < 9999){
                    folio = folio.substring(0, (folio.length())-String.valueOf(Utils.OBSERVABLE_SALE_DATA.getData().size()).length()) + String.valueOf(Utils.OBSERVABLE_SALE_DATA.getData().size()+1);
                }
            }
        }



        folioView.setText(folio);

        clientView.setOnClickListener(this);
        itemView.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        addButon.setOnClickListener(this);

        if(sale != null)
            setupData();
    }

    private void setupRecyclerView(){
        if(observableItem ==null) {
            observableItem = new ObservableItem(new ArrayList<Item>());
            itemObserver = new ItemObserver(observableItem,this);
            observableItem.addObserver(itemObserver);
        }

        saleItemAdapter = new SaleItemAdapter(observableItem.getData(), new ItemClickListener() {
            @Override
            public void onItemClick(Object item) {
                observableItem.updateItem((Item) item);
            }

            @Override
            public void onEditItemClick(Object item) {
                observableItem.removeItem(((Item) item).getId());
            }
        });

        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        itemsRecyclerView.setAdapter(saleItemAdapter);


        payments = new ArrayList<>();
        countedPrice = total/(1+(Utils.FINANCING_RATE*Utils.DEADLINE)/100);
        for (int x = 3; x <= Utils.DEADLINE; x = x+3){
            payments.add(new MonthlyPayment(total, countedPrice, x));
        }

        monthlyPaymentAdapter = new MonthlyPaymentAdapter(payments, new ItemClickListener() {
            @Override
            public void onItemClick(Object item) {

            }

            @Override
            public void onEditItemClick(Object item) {
                MonthlyPayment tempPayment = (MonthlyPayment) item;
                for (MonthlyPayment payment:payments) {
                    if(payment.getPaymentCount() != tempPayment.getPaymentCount()){
                        payment.setSelected(false);
                    }else{
                        payment.setSelected(true);
                    }
                }
                monthlyPaymentAdapter.notifyDataSetChanged();
            }
        });

        monthlyPaymentsRecyclerView.setAdapter(monthlyPaymentAdapter);
        monthlyPaymentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    private boolean checkViews(){

       if(selectedClient == null){
           clientView.setError(getResources().getString(R.string.empty_client));
            clientHasError = true;
       }else{
           clientHasError = false;
           clientView.setError(null);
       }

        for (MonthlyPayment payment:payments) {
            if(payment.isSelected()){
                paymentsHasError = false;
                break;
            }else{
                paymentsHasError = true;
            }
        }

        if(observableItem.getData().size() > 0){
            for (Item item:observableItem.getData()) {
                if(item.isHasError()){
                    itemsHasError = true;
                    break;
                }else{
                    itemsHasError = item.getQuantity() == 0;
                }
            }
        }else{
            itemsHasError = true;
        }

        if(clientHasError || itemsHasError || paymentsHasError){
            Toast.makeText(getContext(), getResources().getString(R.string.empty_data), Toast.LENGTH_SHORT).show();
        }

        return !clientHasError && !itemsHasError && !paymentsHasError;
    }

    private void onSaveGeoFenceSuccess(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if(Utils.OBSERVABLE_SALE_DATA != null){
            Utils.OBSERVABLE_SALE_DATA.updateItem(sale);
        }

        Toast.makeText(getContext(),getResources().getString(R.string.save_changes_ok), Toast.LENGTH_SHORT).show();

        itemIsSaved = true;
        dismiss();
    }

    private void onSaveGeoFenceFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.save_client_error), Toast.LENGTH_LONG).show();
        if(progressDialog.isShowing())
            progressDialog.dismiss();
        itemIsSaved = false;
    }

    private void selectItem(){
        MultiplePicker picker = MultiplePicker.newInstance(getResources().getString(R.string.select_item), TypeRequest.GET_ITEMS, PickerType.SINGLE);
        picker.setListener(new PickerListener() {
            @Override
            public void onSelectSingle(Object object) {
                Item tempItem = new Item(((MultipleObject) object).getObject());
                if(tempItem.getExistence() > 0){
                    selectedItem = tempItem;
                    itemView.setText(selectedItem.getDescription());
                }else{
                    Toast.makeText(getContext(), getResources().getString(R.string.quantity_zero_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSelectList(List<Object> objects) {

            }
        });

        picker.show(getActivity().getSupportFragmentManager(), "ITEM_PICKER");
    }

    private void selectClient(){
        MultiplePicker picker = MultiplePicker.newInstance(getResources().getString(R.string.select_client), TypeRequest.GET_CLIENTS, PickerType.SINGLE);
        picker.setListener(new PickerListener() {
            @Override
            public void onSelectSingle(Object object) {
                selectedClient = new Client(((MultipleObject) object).getObject());
                clientView.setText(selectedClient.getName()+ " " +selectedClient.getLastName() + " " + selectedClient.getLastName());
                clientView.setError(null);
                rfcView.setText(selectedClient.getRfc());
            }

            @Override
            public void onSelectList(List<Object> objects) {

            }
        });

        picker.show(getActivity().getSupportFragmentManager(), "CLIENT_PICKER");
    }

    private void addItem(){
        if(selectedItem != null){
            observableItem.updateItem(selectedItem);
            itemView.getText().clear();
            selectedItem = null;
        }
    }

    private void setupData(){}

    private void addSale(){
        if(checkViews()){
            JSONObject requestData = new JSONObject();
            try{
                requestData.put("client", selectedClient.getId());
                requestData.put("total", total);
                requestData.put("folio", folio);
                for (MonthlyPayment payment:payments) {
                    if(payment.isSelected())
                        requestData.put("paymentCount", payment.getPaymentCount());
                }
                JSONArray items = new JSONArray();
                JSONObject tempItem = new JSONObject();
                for (Item item:observableItem.getData()) {
                    tempItem.put("item", item.getId());
                    tempItem.put("quantity", item.getQuantity());
                    items.put(tempItem);
                }
                requestData.put("items", items);
                if(sale == null)
                    sale = new Sale();

                sale.setClient(selectedClient);
                sale.setTotal(total);
                sale.setFolio(folio);
                sale.setDate(DateTime.now());
                sale.setItems(observableItem.getData());

            }catch (JSONException ex){
                ex.printStackTrace();
            }

            IAsyncApiRequest request = new AsyncApiRequest(getContext());
            request.Post(TypeRequest.SAVE_SALE, requestData, new ApiResponse() {
                @Override
                public void onSuccess(JSONObject data) {
                    try {
                        if (data.getBoolean(Names.IS_VALID)) {
                            sale.setId(data.getJSONObject(Names.DATA).getString("id"));
                            onSaveGeoFenceSuccess();
                        } else {
                            onSaveGeoFenceFailed();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(JSONObject error) {
                    onSaveGeoFenceFailed();
                }

                @Override
                public void onRetry(int number) {

                }

                @Override
                public void onProgress(int percent) {

                }

                @Override
                public void onConnection(boolean isConnected) {
                    if (!isConnected) {
                        Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.client_view:
                selectClient();
                break;
            case R.id.item_view:
                selectItem();
                break;
            case R.id.save_button:
                addSale();
                break;
            case R.id.add_button:
                addItem();
                break;
        }
    }

    private void setupTotals(){
        float tempHitch;
        float tempHitchBonification;
        float tempTotal;

        float tempAmmount = 0;
        for (Item item:observableItem.getData()) {
            tempAmmount = tempAmmount + (item.getQuantity() * item.getPublicPrice());
        }

        tempHitch = (Utils.HITCH/100) * tempAmmount;
        tempHitchBonification = tempHitch * ((Utils.FINANCING_RATE * Utils.DEADLINE)/100);
        tempTotal = tempAmmount - tempHitch - tempHitchBonification;

        hitch = tempHitch;
        hitchBonitication = tempHitchBonification;
        total = tempTotal;

        hitchView.setText(String.valueOf(round(tempHitch,2)));
        hitchBonificationView.setText(String.valueOf(round(tempHitchBonification,2)));
        totalView.setText(String.valueOf(round(tempTotal, 2)));

        countedPrice = total/(1+(Utils.FINANCING_RATE*Utils.DEADLINE)/100);
        for (MonthlyPayment payment:payments) {
            payment.updatePayment(total, countedPrice);
        }

        monthlyPaymentAdapter.notifyDataSetChanged();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void addItems(List<Item> items) {
        saleItemAdapter.notifyDataSetChanged();
        setupTotals();
    }

    @Override
    public void addItem(Item item) {
        saleItemAdapter.notifyDataSetChanged();
        setupTotals();
    }

    @Override
    public void removeItem(Item item) {
        saleItemAdapter.notifyDataSetChanged();
        setupTotals();
    }

}

