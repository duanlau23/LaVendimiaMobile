package duan.test.lavendimia.enums;

import android.content.res.Resources;
import android.net.Uri;

import duan.test.lavendimia.R;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by digiteckno on 18/04/16.
 */
public enum TypeRequest {


    SIGN_UP() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.sign_up_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/security/registerm":
                    "https://" + Utils.API_IP + "/endpoint/security/registerm";
        }
    },SIGN_IN() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.sign_in_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/security/login" :
                    "https://" + Utils.API_IP + "/endpoint/security/login";
        }
    },
    CHECK_STATUS() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.check_status_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/security/checkStatus":
                     "https://" + Utils.API_IP + "/endpoint/security/checkStatus";
        }
    },
    SEND_CODE() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.check_status_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/security/registerm":
                    "https://" + Utils.API_IP + "/endpoint/security/registerm";
        }
    },RECOVERY_ACCOUNT() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.send_code_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/recovery/account":
                    "https://" + Utils.API_IP + "/endpoint/recovery/account";
        }
    },GET_ALL_CLIENTS() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.get_clients_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/clients":
                    "https://" + Utils.API_IP + "/clients";
        }
    },GET_CLIENTS() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.get_clients_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/clients?rexp=" + Uri.encode(Utils.REXP != null ? Utils.REXP :"") + "&rexpfield=name"+
                    (Utils.SKIP != null ? "&page=" + Utils.SKIP : "") :
                    "https://" + Utils.API_IP + "/clients?rexp=" + Uri.encode(Utils.REXP != null ? Utils.REXP :"") + "&rexpfield=name"+
                            (Utils.SKIP != null ? "&page=" + Utils.SKIP : "");
        }
    },SAVE_CLIENT() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.save_client_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/clients":
                    "https://" + Utils.API_IP + "/clients";
        }
    },EDIT_CLIENT() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.edit_client_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/clients/" + Utils.CLIENT_ID:
                    "https://" + Utils.API_IP + "/clients/" + Utils.CLIENT_ID;
        }
    },GET_ALL_ITEMS() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.get_items_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/items" :
                    "https://" + Utils.API_IP + "/items";
        }
    }, GET_ITEMS (){
        public String getErrorText(Resources r) {
            return r.getString(R.string.get_items_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/items?rexp=" + Uri.encode(Utils.REXP != null ? Utils.REXP :"") + "&rexpfield=description"+
                    (Utils.SKIP != null ? "&page=" + Utils.SKIP : "") :
                    "https://" + Utils.API_IP + "/items?rexp=" + Uri.encode(Utils.REXP != null ? Utils.REXP :"") + "&rexpfield=description"+
                            (Utils.SKIP != null ? "&page=" + Utils.SKIP : "");
        }
    },SAVE_ITEM() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.save_item_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/items":
                    "https://" + Utils.API_IP + "/items";
        }
    },EDIT_ITEM() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.edit_item_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/items/" + Utils.ITEM_ID:
                    "https://" + Utils.API_IP + "/items/" + Utils.ITEM_ID;
        }
    },GET_ALL_SALES() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.get_sales_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/get/sales" :
                    "https://" + Utils.API_IP + "/endpoint/get/sales";
        }
    },SAVE_SALE() {
        public String getErrorText(Resources r) {
            return r.getString(R.string.save_sale_error);
        }

        public String getURL() {
            return Utils.DEBUG ? "http://" + Utils.API_IP + ":" + Utils.API_PORT + "/endpoint/sales/add":
                    "https://" + Utils.API_IP + "";
        }
    };

    public abstract String getURL();

    public abstract String getErrorText(Resources r);

}
