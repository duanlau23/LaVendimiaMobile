package duan.test.lavendimia.db;

/**
 * Created by digiteckno on 03/05/16.
 */

import android.provider.BaseColumns;

final class PromtarContract {
    public PromtarContract() {
    }

    /* Inner class that defines the table contents */
    static class ColumnType {
        static final String TEXT = " TEXT";
        static final String INTEGER = " INTEGER";
        static final String BOOLEAN = " INTEGER";
    }


    static abstract class SessionEntry implements BaseColumns {
        static final String SESSION_TABLE = "session";
        static final String TOKEN_KEY = "token";
        static final String USER_ID = "_id";
        static final String NAME = "name";
        static final String LAST_NAME = "lastName";
        static final String EMAIL = "email";
        static final String CREATED_AT = "createdAt";
        static final String VISIBILITY = "visibility";
        static final String USER_NAME = "username";
    }

}