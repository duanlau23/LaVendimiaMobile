package duan.test.lavendimia.db;

/**
 * Created by digiteckno on 03/05/16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.utils.Utils;


public class DbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Xentinel.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PromtarContract.SessionEntry.SESSION_TABLE + " (" +
                    PromtarContract.SessionEntry.USER_ID + TEXT_TYPE + " PRIMARY KEY," +
                    PromtarContract.SessionEntry.TOKEN_KEY + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.NAME + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.USER_NAME + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.LAST_NAME + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.EMAIL + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.CREATED_AT + TEXT_TYPE + " ," +
                    PromtarContract.SessionEntry.VISIBILITY + TEXT_TYPE +
                    " )";


    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PromtarContract.SessionEntry.SESSION_TABLE;


    private static DbHelper instance;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, Utils.DATABASE_VERSION);
        SQLiteDatabase.loadLibs(context);
    }

    static public synchronized DbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DbHelper(context);
        }
        return instance;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        Log.d("sql", SQL_DELETE_ENTRIES + " created");
        onCreate(db);
    }


    public void clearSession() {
        SQLiteDatabase db = this.getWritableDatabase("");
        db.delete(PromtarContract.SessionEntry.SESSION_TABLE, null, null);
        refreshUserData();
        db.close();
    }

    public void refreshUserData(){
        try {
            SQLiteDatabase db = this.getReadableDatabase("");
            Cursor cursor = db.rawQuery("SELECT " + PromtarContract.SessionEntry.TOKEN_KEY + ", " +
                    PromtarContract.SessionEntry.USER_ID + ", " +
                    PromtarContract.SessionEntry.NAME + ", " +
                    PromtarContract.SessionEntry.LAST_NAME + ", " +
                    PromtarContract.SessionEntry.EMAIL + ", " +
                    PromtarContract.SessionEntry.CREATED_AT + ", " +
                    PromtarContract.SessionEntry.USER_NAME + " FROM " + PromtarContract.SessionEntry.SESSION_TABLE + ";", null);
            if (cursor.moveToFirst()) {
                Utils.USER_ID = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.USER_ID));
                Utils.NAME = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.NAME));
                Utils.LAST_NAME = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.LAST_NAME));
                Utils.EMAIL = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.EMAIL));
                Utils.CREATED_AT = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.CREATED_AT));
                Utils.USER_NAME = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.USER_NAME));

            }else{
                Utils.USER_ID = null;
                Utils.NAME = null;
                Utils.LAST_NAME = null;
                Utils.USER_NAME = null;
                Utils.EMAIL = null;
                Utils.CREATED_AT = "";
            }
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean saveUserData(JSONObject data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase("");
            ContentValues values = new ContentValues();
            values.put(PromtarContract.SessionEntry.TOKEN_KEY, data.getJSONObject("data").getString(PromtarContract.SessionEntry.TOKEN_KEY));
            values.put(PromtarContract.SessionEntry.USER_ID, data.getJSONObject("data").getString(PromtarContract.SessionEntry.USER_ID));
            values.put(PromtarContract.SessionEntry.USER_NAME, data.getJSONObject("data").getString(PromtarContract.SessionEntry.USER_NAME));
            values.put(PromtarContract.SessionEntry.NAME, data.getJSONObject("data").get(PromtarContract.SessionEntry.NAME).toString());
            values.put(PromtarContract.SessionEntry.LAST_NAME, data.getJSONObject("data").get(PromtarContract.SessionEntry.LAST_NAME).toString());
            values.put(PromtarContract.SessionEntry.EMAIL, data.getJSONObject("data").getString(PromtarContract.SessionEntry.EMAIL));
            values.put(PromtarContract.SessionEntry.CREATED_AT, data.getJSONObject("data").getString(PromtarContract.SessionEntry.CREATED_AT));

            db.delete(PromtarContract.SessionEntry.SESSION_TABLE, null, null);
            db.insert(PromtarContract.SessionEntry.SESSION_TABLE, null, values);
            db.close();
            refreshUserData();
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("test", e.getMessage());
            return false;
        }
    }

    public void saveToken(JSONObject data) {
        try {
            SQLiteDatabase db = this.getWritableDatabase("");
            ContentValues values = new ContentValues();
            values.put(PromtarContract.SessionEntry.TOKEN_KEY, data.getJSONObject("data").getString(PromtarContract.SessionEntry.TOKEN_KEY));
            values.put(PromtarContract.SessionEntry.USER_ID, data.getJSONObject("data").getString(PromtarContract.SessionEntry.USER_ID));
            values.put(PromtarContract.SessionEntry.USER_NAME, data.getJSONObject("data").getString(PromtarContract.SessionEntry.USER_NAME));

            db.delete(PromtarContract.SessionEntry.SESSION_TABLE, null, null);
            db.insert(PromtarContract.SessionEntry.SESSION_TABLE, null, values);
            db.close();
        } catch (JSONException e) {
            Log.d("ErrorOnSaveToken", e.getMessage());
            e.printStackTrace();
        }
    }

    public String readKey() {
        String result = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase("");
            Cursor cursor = db.rawQuery("SELECT " + PromtarContract.SessionEntry.TOKEN_KEY + " FROM " + PromtarContract.SessionEntry.SESSION_TABLE + ";", null);
            if (cursor.moveToFirst())
                result = cursor.getString(cursor.getColumnIndex(PromtarContract.SessionEntry.TOKEN_KEY));
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.TOKEN = result;
        return result;
    }

}