package duan.test.lavendimia.enums;

/**
 * Created by digiteckno on 23/06/16.
 */
public enum LanguageType {
    ES() {
        @Override
        public String getName(LanguageType type) {
            String res = null;
            switch (type) {
                case ES:
                    res = "Español";
                    break;
                case EN:
                    res = "Spanish";
                    break;
            }
            return res;
        }

        @Override
        public String getAbb() {
            return "es";
        }
    }, EN() {
        @Override
        public String getName(LanguageType type) {
            String res = null;
            switch (type) {
                case ES:
                    res = "Ingles";
                    break;
                case EN:
                    res = "English";
                    break;
            }
            return res;
        }

        @Override
        public String getAbb() {
            return "en";
        }
    };

    public abstract String getName(LanguageType type);

    public abstract String getAbb();
}
