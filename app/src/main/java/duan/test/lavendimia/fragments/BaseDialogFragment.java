package duan.test.lavendimia.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import duan.test.lavendimia.R;
import duan.test.lavendimia.utils.ThemeUtil;

/**
 * Created by duan_ on 28/01/2017.
 */

public class BaseDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, ThemeUtil.getTheme(getContext()));
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

    }
}
