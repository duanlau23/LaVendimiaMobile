package duan.test.lavendimia.custom_views.multiple_picker;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.PickerType;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.MultiplePickerListener;
import duan.test.lavendimia.listeners.PickerListener;
import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.MultipleObject;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

public class MultiplePicker extends DialogFragment implements
        Comparator<Item>,
        View.OnClickListener{

    View mTvLoader;
    int page;
    private EditText searchEditText;
    private RecyclerView countryListView;
    private MultipleAdapter adapter;
    private List<MultipleObject> allObjectsList;
    private List<MultipleObject> filteredObjects;
    private List<MultipleObject> selectedObjects;
    private PickerListener listener;
    View mNoResultsView;
    View mNoConnectionView;
    View mErrorContainer;
    View mTryAgainButton;
    View mOkThanksButton;
    View acceptButton;
    View cancelButton;
    View actionsContainer;
    View v;
    Toast message;
    TypeRequest request;
    PickerType type;
    IAsyncApiRequest asyncApiRequest;

    public static MultiplePicker newInstance(String dialogTitle, TypeRequest request, PickerType type) {
        MultiplePicker picker = new MultiplePicker();
        Bundle bundle = new Bundle();
        bundle.putString("dialogTitle", dialogTitle);
        bundle.putInt("request", request.ordinal());
        bundle.putInt("type", type.ordinal());
        picker.setArguments(bundle);
        return picker;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try{
            Bundle args = getArguments();
            if (args != null) {
                String dialogTitle = args.getString("dialogTitle");
                this.request = TypeRequest.values()[args.getInt("request")];
                this.type = PickerType.values()[args.getInt("type")];
                getDialog().setTitle(dialogTitle);
            }

            setupViews();
            setupRecyclerView();

            //getData(null, 0);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext(),R.style.AppThemeDarkDialog);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.multiple_picker, null);


        builder.setView(v);

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(R.style.slide_animation);
    }

    private void getData(String rexp,int page) {
        if(request == null)
            return;

        Utils.REXP = rexp != null ? rexp : null;
        Utils.SKIP = page > 0 ? String.valueOf(page) : null;

        asyncApiRequest = new AsyncApiRequest(getContext());
        asyncApiRequest.Get(request, "", new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        getDataSuccess(data.getJSONArray("data"));
                    } else {
                        getDataFailed();
                    }
                } catch (JSONException e) {
                    getDataFailed();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                getDataFailed();
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    showMessage(getResources().getString(R.string.connection_error));
                }
            }
        });
    }

    public void getDataFailed() {
        mTvLoader.setVisibility(View.GONE);
        showMessage(getResources().getString(R.string.get_data_error));
    }

    public void showMessage(String text){
        try{
            message.getView().isShown();
            message.setText(text);
        } catch (Exception e) {
            message = Toast.makeText(getContext(),
                    text,
                    Toast.LENGTH_SHORT);
        }
        message.show();
    }

    public void getDataSuccess(JSONArray data) {
        try {
            int count = data.length();
            MultipleObject object;
            JSONObject tempObject;
            allObjectsList.clear();
            for (int i = 0; i < count; i++){
                object = new MultipleObject();
                tempObject = data.getJSONObject(i);
                object.setObject(tempObject);
                object.setName(object.getObject() != null ?
                        (tempObject.has(Names.NAME) ?
                                tempObject.getString(Names.NAME):      tempObject.has(Names.DESCRIPTION) ?
                                tempObject.getString(Names.DESCRIPTION) :
                                "No name") :"No name");

                allObjectsList.add(object);
            }

            filteredObjects.clear();
            filteredObjects.addAll(allObjectsList);
            mTvLoader.setVisibility(View.GONE);

            adapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViews(){
        searchEditText = (EditText) v
                .findViewById(R.id.item_name_view);
        countryListView = (RecyclerView) v
                .findViewById(R.id.country_code_picker_listview);
        mOkThanksButton = v.findViewById(R.id.okay_button);
        mTryAgainButton = v.findViewById(R.id.try_again_button);
        mErrorContainer = v.findViewById(R.id.error_container);
        mNoResultsView = v.findViewById(R.id.no_results);
        mNoConnectionView = v.findViewById(R.id.no_connection);
        acceptButton = v.findViewById(R.id.accept_button);
        cancelButton = v.findViewById(R.id.cancel_button);
        actionsContainer = v.findViewById(R.id.actions_container);

        if(type == PickerType.SINGLE)
            actionsContainer.setVisibility(View.GONE);

        acceptButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        mTryAgainButton.setOnClickListener(this);
        mOkThanksButton.setOnClickListener(this);

        mTvLoader = v.findViewById(R.id.loader_container);

        /*searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                search(s.toString());
            }
        });*/

        searchEditText.addTextChangedListener(new TextWatcher() {
            private final long DELAY = 500;
            Timer timer = new Timer();

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if(s.length() > 2){
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        public void run() {
                            try {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        getData(s.toString(), 0);
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, DELAY);
                }

            }
        });
    }

    private void setupRecyclerView(){
        filteredObjects = new ArrayList<>();
        allObjectsList = new ArrayList<>();
        adapter = new MultipleAdapter(filteredObjects, type, new MultiplePickerListener() {
            @Override
            public void onSelect(Object object) {
                if(type == PickerType.SINGLE){
                    listener.onSelectSingle(object);
                    dismiss();
                }
            }

            @Override
            public void onUnselect(Object object) {

            }
        });
        countryListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        countryListView.setAdapter(adapter);
    }

    public void setListener(PickerListener listener) {
        this.listener = listener;
    }

    private List<Object> getSelectedItems(){
        List<Object> result = new ArrayList<>();
        for (MultipleObject object:allObjectsList) {
            if(object.isSelected())
                result.add(object);
        }

        return result;
    }

    @SuppressLint("DefaultLocale")
    private void search(String text) {
        if (filteredObjects != null)
            filteredObjects.clear();

        for (MultipleObject object : allObjectsList) {
            if (object.getName().toLowerCase(Locale.getDefault())
                    .contains(text.toLowerCase())) {
                filteredObjects.add(object);
            }
        }
        adapter.notifyDataSetChanged();
    }
    @Override
    public int compare(Item lhs, Item rhs) {
        return lhs.getDescription().compareTo(rhs.getDescription());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.accept_button:
                listener.onSelectList(getSelectedItems());
                dismiss();
                break;
            case R.id.cancel_button:
                dismiss();
                break;
            case R.id.try_again_button:
                getData(searchEditText.getText().toString(), page);
                break;
            case R.id.okay_button:
                mNoResultsView.setVisibility(View.GONE);
                break;
        }
    }
}
