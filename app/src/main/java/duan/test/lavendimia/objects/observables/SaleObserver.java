package duan.test.lavendimia.objects.observables;

import java.util.Observable;
import java.util.Observer;

import duan.test.lavendimia.listeners.ObservableItemListener;
import duan.test.lavendimia.listeners.ObservableSaleListener;
import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 26/02/2017.
 */

public class SaleObserver implements Observer {

    private ObservableSale itemData;
    private ObservableSaleListener mListener;

    public SaleObserver(ObservableSale data, ObservableSaleListener listener){
        this.itemData = data;
        this.mListener = listener;
    }

    @Override
    public void update(Observable observable, Object data) {
        try{
            mListener.addItem((Sale) data);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
