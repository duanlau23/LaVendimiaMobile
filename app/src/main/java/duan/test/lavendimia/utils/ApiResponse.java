package duan.test.lavendimia.utils;

import org.json.JSONObject;

/**
 * Created by digiteckno on 02/05/16.
 */
public interface ApiResponse {

    void onSuccess(JSONObject data);

    void onError(JSONObject error);

    void onRetry(int number);

    void onProgress(int percent);

    void onConnection(boolean isConnected);
}
