package duan.test.lavendimia.objects;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by duan_ on 27/02/2017.
 */

public class Item implements Parcelable, Serializable {
    private String id;
    private String folio;
    private String description;
    private String model;
    private String price;
    private float publicPrice;
    private int existence;
    private int quantity;
    private boolean hasError;

    public Item(){}

    public Item(JSONObject data){
        try{
            this.id = data.has(Names.ID) ? data.getString(Names.ID) : "";
            this.folio = data.getString(Names.FOLIO);
            this.description = data.getString(Names.DESCRIPTION);
            this.model = data.has(Names.MODEL) ? data.getString(Names.MODEL) : "";
            this.price = data.getString(Names.PRICE);
            this.publicPrice = Float.valueOf(this.price) * (1+(Utils.FINANCING_RATE * Utils.DEADLINE)/100);
            this.existence = data.getInt(Names.EXISTENCE);
            this.quantity = 0;
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    public Item(JSONObject data, int quantity){
        try{
            this.id = data.has(Names.ID) ? data.getString(Names.ID) : "";
            this.folio = data.getString(Names.FOLIO);
            this.description = data.getString(Names.DESCRIPTION);
            this.model = data.has(Names.MODEL) ? data.getString(Names.MODEL) : "";
            this.price = data.getString(Names.PRICE);
            this.publicPrice = Float.valueOf(this.price) * (1+(Utils.FINANCING_RATE * Utils.DEADLINE)/100);
            this.existence = data.getInt(Names.EXISTENCE);
            this.quantity = 0;
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getExistence() {
        return existence;
    }

    public void setExistence(int existence) {
        this.existence = existence;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPublicPrice() {
        return publicPrice;
    }

    public void setPublicPrice(float publicPrice) {
        this.publicPrice = publicPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.folio);
        dest.writeString(this.description);
        dest.writeString(this.model);
        dest.writeString(this.price);
        dest.writeInt(this.existence);
        dest.writeInt(this.quantity);
        dest.writeInt(this.hasError ? 1:0);
        dest.writeFloat(this.publicPrice);
    }

    protected Item(Parcel in) {
        this.id = in.readString();
        this.folio = in.readString();
        this.description = in.readString();
        this.model = in.readString();
        this.price = in.readString();
        this.existence = in.readInt();
        this.quantity = in.readInt();
        this.hasError = in.readInt() == 1;
        this.publicPrice = in.readFloat();
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}
