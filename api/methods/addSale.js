
var config = require("../../config/config");
var localeUtils = require('../../web/public/locale/utils');
var async = require("async");
module.exports = function(mongoose, userManager, tokenManager, resultUtils, resultType, utils, pModels, privModels, db) {
    return {
        type: 'post',
        route: '/endpoint/sales/add',
        middleware:[function(req,res,next){
            req.tokenOverride = true;
            next();
        },tokenManager.middleware],
        func: function (req, res) {
            var salesModel = pModels["sales"].model;
            var itemsModel = pModels["items"].model;
            var finalValue = new Buffer(req.body.value, 'base64');
            finalValue = JSON.parse(finalValue);
            var newSale = new salesModel(finalValue);
            newSale.save(function (err, sale) {
                if (err) {
                    console.warn(err.message);
                    return res.json(resultUtils.toResult(false, resultType.error, null, err.toString(), {
                        errmsg: err.toString(),
                        code: 'p0002'
                    }));
                }
                var itemsFunctions = [];
                newSale.items.forEach(function (i) {
                    var nFun = function (callback) {
                        itemsModel.findById({_id: i.item}, function (err, item) {
                            if (err) {
                                return callback(err);
                            }
                            item.existence -= i.quantity;
                            item.save(callback)
                        })
                    };
                    itemsFunctions.push(nFun);
                });

                async.series(itemsFunctions, function (err) {
                    if (err) {
                        newSale.delete(function (err2) {
                            if (err2) {
                                return res.json(resultUtils.toResult(false, resultType.error, null, err2.toString(), {
                                    errmsg: err2.toString(),
                                    code: 'p0002'
                                }));
                            } else {
                                if (err) {
                                    return res.json(resultUtils.toResult(false, resultType.error, null, err.toString(), {
                                        errmsg: err.toString(),
                                        code: 'p0002'
                                    }));
                                }
                            }
                        });
                    } else {
                        return res.json(resultUtils.toResult(true, resultType.success, {id: newSale._id}));
                    }
                })
            });
        }
    }
};