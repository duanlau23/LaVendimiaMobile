package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Client;

/**
 * Created by duan_ on 04/12/2016.
 */
 public  class ClientsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView clientName;
    private TextView clientFolio;
    private ItemClickListener listener;
    private Client client;

    ClientsViewHolder(View view, ItemClickListener listener) {
        super(view);
        this.clientName = (TextView) view.findViewById(R.id.client_name);
        this.clientFolio = (TextView) view.findViewById(R.id.client_folio);
        View editClient = view.findViewById(R.id.edit_client);
        editClient.setOnClickListener(this);
        this.listener = listener;
    }

    public void setupData(Client client){
        this.client = client;
        this.clientName.setText(client.getName() + " " +client.getLastName() + " " + client.getMaLastName());
        this.clientFolio.setText(client.getFolio());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_client:
                listener.onEditItemClick(client);
        }
    }
}
