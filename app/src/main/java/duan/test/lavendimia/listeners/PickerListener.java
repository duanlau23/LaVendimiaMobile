package duan.test.lavendimia.listeners;

import java.util.List;

public interface PickerListener {
    void onSelectSingle(Object object);
    void onSelectList(List<Object> objects);
}
