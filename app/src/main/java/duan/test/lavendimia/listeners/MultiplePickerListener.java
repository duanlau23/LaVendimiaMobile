package duan.test.lavendimia.listeners;

public interface MultiplePickerListener {
    void onSelect(Object object);
    void onUnselect(Object object);
}
