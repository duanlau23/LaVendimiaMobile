package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.R;
import duan.test.lavendimia.custom_views.progress_bar.IconRoundCornerProgressBar;
import duan.test.lavendimia.enums.LanguageType;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Utils;

public class SignUpFragment extends DialogFragment {
    private static final String TAG = SignUpFragment.class.getSimpleName();
    boolean passwordHasError = true;
    boolean repeatPasswordHasError = true;
    boolean userHasError = true;
    boolean emailHasError = true;
    boolean nameHasError = true;
    boolean lastNameHasError = true;
    View v;
    String[] partialRegexChecks = {
            ".*[a-z]+.*", // lower
            ".*[A-Z]+.*", // upper
            ".*[0-9]+.*", // digits
            ".*[@#$%+\\-;\\*<>=.,]+.*" // symbols
    };
    private EditText mNameEditText;
    private EditText mLastNameEditText;
    private EditText mPasswordEditText;
    private EditText mUserEditText;
    private EditText mRepeatPasswordEditText;
    private EditText mEmailTextView;
    private IconRoundCornerProgressBar mPasswordProgress;
    View mAlreadyHaveCodeView;
    private View mSignUpButton;
    CheckBox showPassword;
    private ProgressDialog progressDialog;
    SendCodeFragment sendCodeFragment;

    public SignUpFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() == null) {
            return;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = createDialog();

        dialog.getWindow().setWindowAnimations(
                R.style.slide_animation);
        return dialog;
    }

    public boolean isTablet() {
        return (getContext().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() != null) {
            getDialog().getWindow().setWindowAnimations(
                    R.style.slide_animation);
        }

        // set the animations to use on showing and hiding the dialog

        // alternative way of doing it
        //getDialog().getWindow().getAttributes().
        //    windowAnimations = R.style.dialog_animation_fade;

    }

    /**
     * Crea un diÃ¡logo con personalizado para comportarse
     * como formulario de login
     *
     * @return DiÃ¡logo
     */
    public AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.slide_animation);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_sign_up, null);
        builder.setView(v);

        setupViews();


        mSignUpButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(checkViews()){
                            signUp();
                        }
                    }
                }
        );

        return builder.create();
    }

    private void setupViews() {
        mUserEditText = (EditText) v.findViewById(R.id.user);
        mPasswordEditText = (EditText) v.findViewById(R.id.password);
        mRepeatPasswordEditText = (EditText) v.findViewById(R.id.repeat_password);
        mEmailTextView = (EditText) v.findViewById(R.id.email);
        mPasswordProgress = (IconRoundCornerProgressBar) v.findViewById(R.id.password_progress);
        mNameEditText = (EditText) v.findViewById(R.id.name);
        mLastNameEditText = (EditText) v.findViewById(R.id.last_name);
        showPassword = (CheckBox) v.findViewById(R.id.show_password);
        mSignUpButton = v.findViewById(R.id.sign_up);
        mAlreadyHaveCodeView = v.findViewById(R.id.already_have_code_button);

        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);


        showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPasswordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mRepeatPasswordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    mPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mRepeatPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        mAlreadyHaveCodeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendCodeFragment();
            }
        });


        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                changePasswordProgress(s.toString());

                if (s.length() == 0) {
                    mPasswordEditText.setError(null);
                    passwordHasError = true;
                    return;
                }

                if (s.length() < 8 && s.length() > 0) {
                    mPasswordEditText.setError(getResources().getString(R.string.contains_8characters));
                    passwordHasError = true;
                    return;
                }


                if (!s.toString().matches(partialRegexChecks[1])) {
                    mPasswordEditText.setError(getResources().getString(R.string.contains_mayus));
                    passwordHasError = true;
                    return;
                }

                if (!s.toString().matches(partialRegexChecks[2])) {
                    mPasswordEditText.setError(getResources().getString(R.string.contains_number));
                    passwordHasError = true;
                    return;
                }

                passwordHasError = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mRepeatPasswordEditText.getText().length() > 0) {
                    String a = mPasswordEditText.getText().toString();
                    String b = mRepeatPasswordEditText.getText().toString();
                    if (!a.equals(b)) {
                        mRepeatPasswordEditText.setError(getResources().getString(R.string.password_no_match));
                        repeatPasswordHasError = true;
                        return;
                    } else {
                        mRepeatPasswordEditText.setError(null);
                        repeatPasswordHasError = false;
                    }
                } else {
                    repeatPasswordHasError = true;
                }

                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    mPasswordEditText.setText(result);
                    mPasswordEditText.setSelection(result.length());
                }
            }
        });

        mRepeatPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mRepeatPasswordEditText.setError(null);
                    repeatPasswordHasError = true;
                    return;
                }


                String a = mPasswordEditText.getText().toString();
                String b = mRepeatPasswordEditText.getText().toString();
                if (!a.equals(b)) {
                    mRepeatPasswordEditText.setError(getResources().getString(R.string.password_no_match));
                    repeatPasswordHasError = true;
                    return;
                } else {
                    mRepeatPasswordEditText.setError(null);
                }

                repeatPasswordHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mNameEditText.setError(null);
                    nameHasError = true;
                    return;
                }


                if (s.toString().matches(partialRegexChecks[2])) {
                    mNameEditText.setError(getResources().getString(R.string.start_wnumber));
                    nameHasError = true;
                    return;
                }
                if (s.toString().matches(partialRegexChecks[3])) {
                    mNameEditText.setError(getResources().getString(R.string.has_ilegal_character));
                    nameHasError = true;
                    return;
                }

                nameHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mLastNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mLastNameEditText.setError(null);
                    lastNameHasError = true;
                    return;
                }


                if (s.toString().matches(partialRegexChecks[2])) {
                    mLastNameEditText.setError(getResources().getString(R.string.start_wnumber));
                    lastNameHasError = true;
                    return;
                }
                if (s.toString().matches(partialRegexChecks[3])) {
                    mLastNameEditText.setError(getResources().getString(R.string.has_ilegal_character));
                    lastNameHasError = true;
                    return;
                }

                lastNameHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mUserEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mUserEditText.setError(null);
                    userHasError = true;
                    return;
                }


                if (s.subSequence(0, 1).toString().matches(partialRegexChecks[2])) {
                    mUserEditText.setError(getResources().getString(R.string.start_wnumber));
                    userHasError = true;
                    return;
                }

                if (s.length() < 6 && s.length() > 0) {
                    mUserEditText.setError(getResources().getString(R.string.contains_characters));
                    userHasError = true;
                    return;
                }

                if (s.toString().matches(partialRegexChecks[3])) {
                    mUserEditText.setError(getResources().getString(R.string.has_ilegal_character));
                    userHasError = true;
                    return;
                }

                userHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    mUserEditText.setText(result);
                    mUserEditText.setSelection(result.length());
                }
            }
        });

        mEmailTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mEmailTextView.setError(null);
                    emailHasError = true;
                    return;
                }

                if (!s.toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    mEmailTextView.setError(getResources().getString(R.string.invalid_email));
                    emailHasError = true;
                    return;
                }

                emailHasError = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().replaceAll(" ", "");
                if (!s.toString().equals(result)) {
                    mEmailTextView.setText(result);
                    mEmailTextView.setSelection(result.length());
                }
            }
        });
    }

    private void changePasswordProgress(String password) {
        int progress = (int) calculateScore(password);

        progress = progress > 100 ? 100 : progress;

        if (progress < 40) {
            mPasswordProgress.setProgressColor(getResources().getColor(R.color.incorrect_password));
            mPasswordProgress.setProgressBackgroundColor(getResources().getColor(R.color.incorrect_password_background));
            mPasswordProgress.setIconBackgroundColor(getResources().getColor(R.color.incorrect_password_background));
            mPasswordProgress.setIconImageResource(R.drawable.ic_vn_open);
        } else if (progress >= 40 && progress < 80) {
            mPasswordProgress.setProgressColor(getResources().getColor(R.color.weak_password_color));
            mPasswordProgress.setProgressBackgroundColor(getResources().getColor(R.color.weak_password_color_background));
            mPasswordProgress.setIconBackgroundColor(getResources().getColor(R.color.weak_password_color_background));
            mPasswordProgress.setIconImageResource(R.drawable.ic_vn_semi_open);
        } else if (progress >= 80) {
            mPasswordProgress.setProgressColor(getResources().getColor(R.color.strong_password_color));
            mPasswordProgress.setProgressBackgroundColor(getResources().getColor(R.color.strong_password_color_background));
            mPasswordProgress.setIconBackgroundColor(getResources().getColor(R.color.strong_password_color_background));
            mPasswordProgress.setIconImageResource(R.drawable.ic_vn_closed);
        }


        mPasswordProgress.setProgress((float) progress);
    }

    private boolean checkViews() {

        if (mNameEditText.length() == 0) {
            mNameEditText.setError(getResources().getString(R.string.empty_name));
            nameHasError = true;
        }

        if (mLastNameEditText.length() == 0) {
            mLastNameEditText.setError(getResources().getString(R.string.empty_last_name));
            lastNameHasError = true;
        }

        if (mEmailTextView.length() == 0) {
            mEmailTextView.setError(getResources().getString(R.string.empty_email));
            emailHasError = true;
        }

        if (mUserEditText.length() == 0) {
            mUserEditText.setError(getResources().getString(R.string.empty_user));
            userHasError = true;
        }


        if (mUserEditText.length() < getResources().getInteger(R.integer.user_min_length) && mUserEditText.length() > 0) {
            mUserEditText.setError(getResources().getString(R.string.contains_characters));
            userHasError = true;
        }


        if (mPasswordEditText.length() == 0) {
            mPasswordEditText.setError(getResources().getString(R.string.empty_password));
            passwordHasError = true;
        }

        if (mPasswordEditText.length() < getResources().getInteger(R.integer.password_min_length) && mPasswordEditText.length() > 0) {
            mPasswordEditText.setError(getResources().getString(R.string.contains_8characters));
            passwordHasError = true;
        }

        if (mRepeatPasswordEditText.length() == 0) {
            mRepeatPasswordEditText.setError(getResources().getString(R.string.empty_password));
            repeatPasswordHasError = true;
        }

        if (mRepeatPasswordEditText.length() < getResources().getInteger(R.integer.password_min_length) && mRepeatPasswordEditText.length() > 0) {
            mRepeatPasswordEditText.setError(getResources().getString(R.string.contains_8characters));
            repeatPasswordHasError = true;
        }

        return !(userHasError || passwordHasError || nameHasError || repeatPasswordHasError || lastNameHasError || emailHasError);
    }

    float calculateScore(String password) {
        int passwordStrenght = 0;

        for (char character : password.toCharArray()) {
            String temp = String.valueOf(character);
            if (temp.matches(partialRegexChecks[0])) {
                passwordStrenght = passwordStrenght + 5;
            } else if (temp.matches(partialRegexChecks[1])) {
                passwordStrenght = passwordStrenght + 8;
            } else if (temp.matches(partialRegexChecks[2])) {
                passwordStrenght = passwordStrenght + 10;
            } else if (temp.matches(partialRegexChecks[3])) {
                passwordStrenght = passwordStrenght + 10;
            } else {
                if (Utils.LANGUAGE == LanguageType.ES) {
                    mPasswordEditText.setError(getResources().getString(R.string.character) + " '" + character + "' " + getResources().getString(R.string.invalid));
                } else if (Utils.LANGUAGE == LanguageType.EN) {
                    mPasswordEditText.setError(getResources().getString(R.string.invalid) + " '" + character + "' " + getResources().getString(R.string.character));
                }
                passwordHasError = true;
            }

        }

        return (float) passwordStrenght;
    }

    private void signUp() {

        progressDialog.setMessage(getResources().getString(R.string.creating_account_text));
        progressDialog.show();

        String name = mNameEditText.getText().toString();
        String lastName = mLastNameEditText.getText().toString();
        String userName = mUserEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();
        String email = mEmailTextView.getText().toString();

        JSONObject data = new JSONObject();
        try {
            data.put("name", name);
            data.put("lastName", lastName);
            data.put("email", email);
            data.put("username", userName);
            data.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        onSignupSuccess(data);
    }

    public void onSignupSuccess(JSONObject data) {
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        request.Post(TypeRequest.SIGN_UP, data, new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        showSendCodeFragment();
                    } else {
                        onSigntupFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                onSigntupFailed();
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    onSigntupFailed();
                }
            }
        });
    }

    public void onSigntupFailed() {
        progressDialog.dismiss();
    }

    private void showSendCodeFragment() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (sendCodeFragment == null) {
            sendCodeFragment = new SendCodeFragment();
        }
        if (sendCodeFragment.isVisible())
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();
        sendCodeFragment = new SendCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("width", v.getMeasuredWidth());
        bundle.putInt("height", v.getMeasuredHeight());
        sendCodeFragment.setArguments(bundle);
        sendCodeFragment.show(fm, "login_fragment");
    }
}
