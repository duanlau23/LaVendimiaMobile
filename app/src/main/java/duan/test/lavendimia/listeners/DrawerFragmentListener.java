package duan.test.lavendimia.listeners;

import android.view.MenuItem;

public interface DrawerFragmentListener {
        void onDrawerItemClick(MenuItem item);
    }