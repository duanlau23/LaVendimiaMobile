package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 04/12/2016.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {
    private List<Item> itemList;
    private ItemClickListener listener;

    public ItemAdapter(List<Item> itemList, ItemClickListener listener){
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_item, parent, false);
        return new ItemViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {

        Item item = getItem(position);

        if(item == null){
            return;
        }

        holder.setupData(item);

    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private Item getItem(int i){
        try{
            return itemList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
