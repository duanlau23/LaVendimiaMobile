module.exports =  function(mongoose,connection){
    var saleSchema = {
        folio: {type:String, index: {unique:true}, required:true},
        total: {type:Number, required:true},
        client: {type: mongoose.Schema.Types.ObjectId, ref: 'clients', required:true},
        items :  [{
            item: {type: mongoose.Schema.Types.ObjectId, ref: 'items'},
            quantity: {type:Number, required:true}
        }],
        paymentCount: {type:Number, require:true, default:1},
        date: {type: Date, default: Date.now}
    };

    return {
        model : connection.model('sales',saleSchema),
        schema : {
            folio: "required",
            total: "required",
            client: "required",
            items: "required",
            paymentCount: "required"
        }
    }
};