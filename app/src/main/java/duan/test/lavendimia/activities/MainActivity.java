package duan.test.lavendimia.activities;

import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import duan.test.lavendimia.R;
import duan.test.lavendimia.db.DbHelper;
import duan.test.lavendimia.enums.FragmentAction;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.fragments.ClientsFragment;
import duan.test.lavendimia.fragments.DrawerFragment;
import duan.test.lavendimia.fragments.EditClientFragment;
import duan.test.lavendimia.fragments.ItemsFragment;
import duan.test.lavendimia.fragments.SalesFragment;
import duan.test.lavendimia.fragments.SettingsFragment;
import duan.test.lavendimia.listeners.CheckStatusListener;
import duan.test.lavendimia.listeners.DrawerFragmentListener;
import duan.test.lavendimia.listeners.FragmentActionListener;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

public class MainActivity extends BaseActivity implements
        FragmentActionListener,
        CheckStatusListener,
        DrawerFragmentListener,
        View.OnClickListener{

    ProgressDialog progressDialog;
    boolean checkStatusSuccess;
    boolean checkStatusEnd;
    Toolbar toolbar;
    DrawerFragment drawerFragment;
    SettingsFragment settingsFragment;
    DrawerLayout drawer;
    TextView dateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeUI();
    }

    public void InitializeUI()
    {
        setupViews();
        setupDrawer();
    }

    private void setupDrawer(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();



        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        try {
            if (drawerFragment == null) {
                drawerFragment = new DrawerFragment();
            }
            if (drawerFragment.isAdded())
                return;

            ft.replace(R.id.nav_view, drawerFragment).commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        //checkStatus();

    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        InitializeUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private void checkStatus() {

        if(progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setMessage(getResources().getString(R.string.validating_session));
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        IAsyncApiRequest request = new AsyncApiRequest(this.getApplicationContext());
        request.Post(TypeRequest.CHECK_STATUS, new JSONObject(), new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    DbHelper helper = DbHelper.getInstance(getApplicationContext());
                    if (data.getBoolean("isValid")) {
                        helper.saveUserData(data);
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();
                        onCheckStatusSuccess();
                    } else {
                        helper.clearSession();
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();
                            onCheckStatusFailed();
                        }
                    } catch (JSONException e) {
                        checkStatusFailed();
                        e.printStackTrace();
                    }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    onCheckStatusFailed();
                }
            }

            @Override
            public void onRetry(int number) {
                onCheckStatusRepeat();
            }

            @Override
            public void onProgress(int percent) {
                if(progressDialog.isShowing())
                    progressDialog.setProgress(percent);
            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    DbHelper helper = DbHelper.getInstance(getApplicationContext());
                    helper.refreshUserData();
                    if (Utils.USER_ID != null) {
                        checkStatusSuccess();
                    } else {
                        checkStatusFailed();
                    }
                }
            }
        });
    }

    private void setupViews(){
        dateView = (TextView) findViewById(R.id.date_view);
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy ");
        dateView.setText(mdformat.format(Calendar.getInstance().getTime()));
    }



    private void showLogin(){
        try{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void showClients(){
        if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) != null)
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fragment_container)).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new ClientsFragment()).commit();
        drawer.closeDrawer(GravityCompat.START);
    }

    private void showItems(){
        if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) != null)
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fragment_container)).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new ItemsFragment()).commit();
        drawer.closeDrawer(GravityCompat.START);
    }

    private void showSales(){
        if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) != null)
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.fragment_container)).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new SalesFragment()).commit();
        drawer.closeDrawer(GravityCompat.START);
    }

    private void showSettings(){
        if (settingsFragment == null) {
            settingsFragment = new SettingsFragment();
        }

        if(settingsFragment.isVisible())
            return;

        if (settingsFragment.isAdded())
            return;

        FragmentManager fm = getSupportFragmentManager();
        settingsFragment = new SettingsFragment();
        settingsFragment.show(fm, "editItemFragment");
        drawer.closeDrawer(GravityCompat.START);
    }

    private void logOut(){
        if(progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

        progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getResources().getString(R.string.logout_text));
        progressDialog.show();
        DbHelper db = new DbHelper(this);
        db.clearSession();
        progressDialog.dismiss();
        checkStatus();
    }

    private void checkStatusFailed() {
        if(progressDialog.isShowing())
            progressDialog.dismiss();
        showLogin();
    }

    private void checkStatusSuccess() {
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        drawerFragment.changeUserData();
    }




    @Override
    public void onFragmentAction(FragmentAction action) {
        switch (action){
            case LOGIN_SUCCESS:
                break;
            case LOGIN_FAILED:
                break;
            case LOG_IN:
                checkStatus();
                break;
            case LOST_PASSWORD:
                break;
            case CHANGE_LANGUAJE:
                break;
        }
    }

    @Override
    public void onCheckStatusSuccess() {
        checkStatusSuccess = true;
        checkStatusSuccess();
        checkStatusEnd = true;
    }

    @Override
    public void onCheckStatusFailed() {
        checkStatusSuccess = false;
        checkStatusFailed();

        checkStatusEnd = true;
    }

    @Override
    public void onCheckStatusRepeat() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onDrawerItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_sales:
                showSales();
                break;
            case R.id.nav_clients:
                showClients();
                break;
            case R.id.nav_items:
                showItems();
                break;
            case R.id.nav_settings:
                showSettings();
                break;
            case R.id.nav_log_out:
                logOut();
                break;
        }
    }
}
