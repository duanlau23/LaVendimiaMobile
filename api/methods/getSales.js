/**
 * Created by ben on 6/28/2016.
 */

var config = require('../../config/config');

module.exports = function(mongoose, userManager, tokenManager, resultUtils, resultType, utils, pModels, privModels,  db){
    return {
        type: 'get',
        route: '/endpoint/get/sales',
        middleware: [function(req,res,next){
            req.tokenOverride = true;
            next();
        },tokenManager.middleware],
        func: function (req, res) {
            var salesModel = db.model("sales");
            salesModel.find().select("_id client total folio items paymentCount date").populate(
                [{path:"items.item",model:"items", select:"folio description model price existence"},
                    {path: "client", model:"clients", select:"folio name lastName maLastName rfc"}]).exec(function (err, doc) {
                if (!err) {
                    res.json(resultUtils.toResult(true, resultType.success, {items:doc, totalCount:0}));
                } else {
                    console.warn(err);
                    res.json(resultUtils.toResult(true, resultType.success, {items:[], totalCount:0}));
                }
            });

        }
    };
};