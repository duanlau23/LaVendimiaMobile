package duan.test.lavendimia.objects;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import duan.test.lavendimia.utils.Names;

/**
 * Created by duan_ on 27/02/2017.
 */

public class Sale implements Parcelable {
    private String id;
    private String folio;
    private Client client;
    private float total;
    private DateTime date;
    private List<Item> items;
    private int paymentCount;

    public Sale(){}

    public Sale(JSONObject data){
        try{
            this.id = data.has(Names.ID) ? data.getString(Names.ID) : "";
            this.folio = data.getString(Names.FOLIO);
            this.client = new Client(data.getJSONObject(Names.CLIENT));
            this.total = Float.parseFloat(data.getString(Names.TOTAL));
            this.paymentCount = data.getInt(Names.PAYMENT_COUNT);
            this.date = ISODateTimeFormat.dateTime().parseDateTime(data.getString("date"));
            this.items = new ArrayList<>();
            if(data.has(Names.ITEMS)){
                Item item;
                for (int x = 0; x < data.getJSONArray(Names.ITEMS).length();x++){
                    item = new Item(data.getJSONArray(Names.ITEMS).getJSONObject(x).getJSONObject("item"),
                                    data.getJSONArray(Names.ITEMS).getJSONObject(x).getInt("quantity"));
                    this.items.add(item);
                }
            }
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.folio);
        dest.writeSerializable(this.client);
        dest.writeFloat(this.total);
        dest.writeSerializable(this.date);
        dest.writeInt(this.paymentCount);
        dest.writeTypedList(this.items);

    }

    protected Sale(Parcel in) {
        this.id = in.readString();
        this.folio = in.readString();
        this.client = (Client) in.readSerializable();
        this.total = in.readFloat();
        this.date = (DateTime) in.readSerializable();
        this.paymentCount = in.readInt();
        in.readTypedList(this.items, Item.CREATOR);
    }

    public static final Parcelable.Creator<Sale> CREATOR = new Parcelable.Creator<Sale>() {
        @Override
        public Sale createFromParcel(Parcel source) {
            return new Sale(source);
        }

        @Override
        public Sale[] newArray(int size) {
            return new Sale[size];
        }
    };
}
