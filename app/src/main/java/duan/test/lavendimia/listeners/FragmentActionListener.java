package duan.test.lavendimia.listeners;

import duan.test.lavendimia.enums.FragmentAction;

public interface FragmentActionListener {
        void onFragmentAction(FragmentAction action);
    }