package duan.test.lavendimia.objects.observables;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import duan.test.lavendimia.objects.Client;

/**
 * Created by duan_ on 26/02/2017.
 */

public class ObservableClient extends Observable {

    private List<Client> data = new ArrayList<>();

    public ObservableClient(List<Client> data)
    {
        this.data = data;
    }


    public void setValue(List<Client> data)
    {
        this.data.addAll(data);
        setChanged();
        notifyObservers();
    }

    void addItem(Client item){
        this.data.add(item);
        setChanged();
        notifyObservers(item);
    }

    public void updateItem(Client item){
        boolean isFound = false;
        for (int x = 0;x<this.data.size();x++){
            if(this.data.get(x).getId().equals(item.getId())) {
                this.data.set(x, item);
                isFound = true;
                break;
            }
        }
        if(isFound) {
            setChanged();
            notifyObservers();
        }else{
            addItem(item);
        }
    }

    public void removeItem(String devideId){
        for (Client reportD:this.data) {
            if(reportD.getId().equals(devideId)) {
                this.data.remove(reportD);
                break;
            }
        }
        setChanged();
        notifyObservers();
    }

    public List<Client> getData(){
        return this.data;
    }

    public void removeItems(List<Client> data){
        this.data.removeAll(data);
        setChanged();
        notifyObservers();
    }
    public List<Client>  getValue()
    {
        return data;
    }

}