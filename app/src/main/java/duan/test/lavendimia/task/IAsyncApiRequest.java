package duan.test.lavendimia.task;

import org.json.JSONObject;

import java.io.File;

import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.utils.ApiResponse;
/**
 * Created by digiteckno on 10/08/16.
 */
public interface IAsyncApiRequest {
    boolean Put(TypeRequest request, JSONObject data, ApiResponse respuesta);

    boolean Post(TypeRequest request, JSONObject data, ApiResponse respuesta);

    boolean Delete(TypeRequest request, JSONObject data, ApiResponse respuesta);

    boolean Get(TypeRequest request, String params, ApiResponse respuesta);

    void GetSync(TypeRequest request, String params, ApiResponse respuesta);

    boolean UploadImage(TypeRequest request, String imageUrl, ApiResponse respuesta);

    File createTempFile(String namePart, int byteSize);
}
