package duan.test.lavendimia.custom_views.multiple_picker;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.PickerType;
import duan.test.lavendimia.listeners.MultiplePickerListener;
import duan.test.lavendimia.objects.MultipleObject;
/**
 * Created by duan_ on 04/12/2016.
 */

public class MultipleAdapter extends RecyclerView.Adapter<MultipleViewHolder> {
    private List<MultipleObject> objectList;
    private MultiplePickerListener listener;
    private PickerType type;

    public MultipleAdapter(List<MultipleObject> geoFenceList,PickerType type, MultiplePickerListener listener){
        this.objectList = geoFenceList;
        this.listener = listener;
        this.type = type;
    }

    @Override
    public MultipleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.multiple_picker_item, parent, false);
        return new MultipleViewHolder(view, listener, type);
    }

    @Override
    public void onBindViewHolder(final MultipleViewHolder holder, int position) {

        MultipleObject object = getItem(position);
        holder.setupViews(object);
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    private MultipleObject getItem(int i){
        try{
            return objectList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
