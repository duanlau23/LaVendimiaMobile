package duan.test.lavendimia.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.custom_views.custom_recyclerview.ItemAdapter;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.listeners.ObservableItemListener;
import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.observables.ItemObserver;
import duan.test.lavendimia.objects.observables.ObservableItem;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by duan_ on 25/02/2017.
 */

public class ItemsFragment extends Fragment implements
        ItemClickListener,
        ObservableItemListener{

    RecyclerView itemsRecyclerView;
    List<Item> itemList;
    ItemAdapter itemAdapter;
    ProgressDialog progressDialog;
    EditItemFragment editItemFragment;
    ItemObserver itemObserver;
    View editItemView;
    View v;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);}

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.items_fragment, container, false);
        setupViews();
        setupRecyclerView();
        getItems();
        return v;
    }

    private void setupViews(){
        itemsRecyclerView = (RecyclerView) v.findViewById(R.id.item_list);
        editItemView = v.findViewById(R.id.edit_item);

        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.loading_items));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
    }

    private void setupRecyclerView(){

        editItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editItem(null);
            }
        });

        if(Utils.OBSERVABLE_ITEM_DATA == null) {
            itemList = new ArrayList<>();
        }else{
            itemList = new ArrayList<>();
            itemList.addAll(Utils.OBSERVABLE_ITEM_DATA.getData());
        }

        itemObserver = new ItemObserver(Utils.OBSERVABLE_ITEM_DATA, this);
        Utils.OBSERVABLE_ITEM_DATA = new ObservableItem(itemList);
        Utils.OBSERVABLE_ITEM_DATA.addObserver(itemObserver);

        itemAdapter = new ItemAdapter(itemList, this );
        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        itemsRecyclerView.setAdapter(itemAdapter);
    }


    private void editItem(Item item){
        if (editItemFragment == null) {
            editItemFragment = new EditItemFragment();
        }

        if(editItemFragment.isVisible())
            return;

        if (editItemFragment.isAdded())
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();
        editItemFragment = new EditItemFragment();
        if(item != null)
        {
            Bundle data = new Bundle();
            data.putParcelable("sale", item);
            editItemFragment.setArguments(data);
        }

        editItemFragment.show(fm, "editItemFragment");
    }

    private void getItems(){
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        itemList.clear();
        progressDialog.show();
        request.Get(TypeRequest.GET_ALL_ITEMS, "", new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean(Names.IS_VALID)) {
                        onGetGeoFencesSuccess(data.getJSONArray("data"));
                    } else {
                        onGetGeoFencesFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    onGetGeoFencesFailed();
                }
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        });
    }

    private void onGetGeoFencesSuccess(JSONArray data){
        try{
            Item item;
            for (int x = 0; x < data.length(); x++){
                item = new Item(data.getJSONObject(x));
                itemList.add(item);
            }
            dismissDialog();
            itemAdapter.notifyDataSetChanged();
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    private void onGetGeoFencesFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.get_items_error), Toast.LENGTH_LONG).show();
        dismissDialog();
    }

    private void dismissDialog(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onItemClick(Object item) {
        editItem((Item) item);
    }

    @Override
    public void onEditItemClick(Object item) {
        editItem((Item) item);
    }

    @Override
    public void addItems(List<Item> items) {
        itemAdapter.notifyDataSetChanged();
    }

    @Override
    public void addItem(Item data) {
        itemAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeItem(Item item) {
        itemAdapter.notifyDataSetChanged();
    }
}
