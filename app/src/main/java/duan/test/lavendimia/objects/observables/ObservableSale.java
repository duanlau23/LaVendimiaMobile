package duan.test.lavendimia.objects.observables;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 26/02/2017.
 */

public class ObservableSale extends Observable {

    private List<Sale> data = new ArrayList<>();

    public ObservableSale(List<Sale> data)
    {
        this.data = data;
    }


    public void setValue(List<Sale> data)
    {
        this.data.addAll(data);
        setChanged();
        notifyObservers();
    }

    void addItem(Sale item){
        this.data.add(item);
        setChanged();
        notifyObservers(item);
    }

    public void updateItem(Sale item){
        boolean isFound = false;
        for (int x = 0;x<this.data.size();x++){
            if(this.data.get(x).getId().equals(item.getId())) {
                this.data.set(x, item);
                isFound = true;
                break;
            }
        }
        if(isFound) {
            setChanged();
            notifyObservers();
        }else{
            addItem(item);
        }
    }

    public void removeItem(String devideId){
        for (Sale reportD:this.data) {
            if(reportD.getId().equals(devideId)) {
                this.data.remove(reportD);
                break;
            }
        }
        setChanged();
        notifyObservers();
    }

    public List<Sale> getData(){
        return this.data;
    }

    public void removeItems(List<Sale> data){
        this.data.removeAll(data);
        setChanged();
        notifyObservers();
    }
    public List<Sale>  getValue()
    {
        return data;
    }

}