/**
 * Created by duan_ on 26/02/2017.
 */
/**
 * Created by ben on 09/11/2015.
 */
module.exports =  function(mongoose,connection){
    var itemsSchema  = {
        folio: {type:String, index: {unique:true}, required:true},
        description: {type:String, index: {unique:false}, required:true},
        model: {type: String, index: {unique:false}, required:false},
        price: {type:String, index: {unique:false}, required:true},
        existence: {type:Number, index: {unique:false}, required:true}
    };

    return {
        model : connection.model('items',itemsSchema),
        schema : {
            folio: "required",
            description: "required",
            price: "required",
            existence: "required"
        }
    }
};