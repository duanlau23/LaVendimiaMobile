package duan.test.lavendimia.utils;

import duan.test.lavendimia.enums.LanguageType;
import duan.test.lavendimia.objects.observables.ObservableClient;
import duan.test.lavendimia.objects.observables.ObservableItem;
import duan.test.lavendimia.objects.observables.ObservableSale;

/**
 * Created by digiteckno on 09/10/16.
 */
public class Utils {
    public static boolean DEBUG = true;
    public static int DATABASE_VERSION = 4;

    public final static String X_APP_ID_HEADER = "x-application-id";
    public final static String X_SESSION_HEADER = "x-c-session";
    public final static String X_SESSION_REMEMBER_HEADER = "x-c-session-remember";

    public final static String SHARED_THEME_NAME = "theme";
    public final static int THEME_DARK = 1;


    public final static int DAYS_UNTIL_PROMPT = 0;
    public final static int LAUNCHES_UNTIL_PROMPT = 0;

    //public static String API_IP = "52.40.4.207";
    //public static String API_PORT = "3000";

    public static String API_IP = "api.duanlau.pro";
    public static String API_PORT = "80";



    public static String APLICATION_ID = "565e034e2086880d72de6c3f";

    public static LanguageType LANGUAGE = LanguageType.ES;

    public static String SERVER_KEY = "AIzaSyBvlN5q3cnWZRt45xg8gP9TM9Tz_MmMIOs";
    public static String GOOGLE_ROUTE_URL;

    public static String REXP;
    public static String PAGE;
    public static String SKIP;
    public static String SEARCH;

    public static String TOKEN;
    public static String CLIENT_ID;
    public static String ITEM_ID;
    public static String SALE_ID;
    public static ObservableClient OBSERVABLE_CLIENT_DATA;
    public static ObservableItem OBSERVABLE_ITEM_DATA;
    public static ObservableSale OBSERVABLE_SALE_DATA;

    public static float HITCH;
    public static float FINANCING_RATE;
    public static int DEADLINE;

    public static String REPORT_ID;
    public static String USER_ID;
    public static String USER_NAME;
    public static String NAME;
    public static String LAST_NAME;
    public static String EMAIL;
    public static String CREATED_AT;
}
