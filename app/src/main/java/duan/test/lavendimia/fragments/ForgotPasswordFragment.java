package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;


public class ForgotPasswordFragment extends BaseDialogFragment {
    private ProgressDialog progressDialog;
    private EditText mEmailTextView;
    private Button mSendEmail;
    private Button mAlreadyHaveCode;
    private View v;
    private boolean emailHasError = true;
    SendCodeFragment sendCodeFragment;

    public ForgotPasswordFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createLoginDialogo();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);
        // alternative way of doing it
        //getDialog().getWindow().getAttributes().
        //    windowAnimations = R.style.dialog_animation_fade;

    }

    /**
     * Crea un diÃ¡logo con personalizado para comportarse
     * como formulario de login
     *
     * @return DiÃ¡logo
     */
    public AlertDialog createLoginDialogo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.slide_animation);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_forgot_password, null);

        setupViews();

        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        mEmailTextView = (EditText) v.findViewById(R.id.forgot_email);
        mSendEmail = (Button) v.findViewById(R.id.send_email_button);
        mAlreadyHaveCode = (Button) v.findViewById(R.id.already_have_code_button);

        mSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkViews())
                    sendCode();
            }
        });

        mAlreadyHaveCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSendCode();
            }
        });

        mEmailTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mEmailTextView.setError(null);
                    emailHasError = true;
                    return;
                }

                if (!s.toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    mEmailTextView.setError(getResources().getString(R.string.invalid_email));
                    emailHasError = true;
                    return;
                }

                emailHasError = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkViews();
            }
        });
    }

    private void showSendCode(){
        if (sendCodeFragment == null) {
            sendCodeFragment = new SendCodeFragment();
        }
        if (sendCodeFragment.isVisible())
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();
        sendCodeFragment = new SendCodeFragment();
        sendCodeFragment.show(fm, "login_fragment");
    }

    private boolean checkViews() {
        if (mEmailTextView.getText().length() == 0) {
            mEmailTextView.setError(getResources().getString(R.string.empty_email));
            emailHasError = true;
        }

        mSendEmail.setEnabled(!emailHasError);


        return emailHasError;
    }

    private void sendCode() {


        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getResources().getString(R.string.sending_code_text));
        progressDialog.show();


        JSONObject data = new JSONObject();
        try {
            data.put("value", mEmailTextView.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        onSendCodeSuccess(data);
    }

    public void onSendCodeSuccess(JSONObject data) {
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        request.Post(TypeRequest.RECOVERY_ACCOUNT, data, new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        openSendCodeFragment();
                    } else {
                        onSendCodeFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(JSONObject error) {
                onSendCodeFailed();
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), TypeRequest.RECOVERY_ACCOUNT.getErrorText(getResources()), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void openSendCodeFragment() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        FragmentManager fm = getActivity().getSupportFragmentManager();
        SendCodeFragment newFragment = new SendCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("email", mEmailTextView.getText().toString());
        newFragment.setArguments(bundle);
        newFragment.show(fm, "dialogfragment_send_email");
    }

    public void onSendCodeFailed() {
        progressDialog.dismiss();
    }
}
