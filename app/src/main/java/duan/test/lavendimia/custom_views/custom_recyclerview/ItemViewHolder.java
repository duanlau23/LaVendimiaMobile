package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 04/12/2016.
 */
 public  class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView itemDescription;
    private TextView itemFolio;
    private ItemClickListener listener;
    private Item item;

    ItemViewHolder(View view, ItemClickListener listener) {
        super(view);
        this.itemDescription = (TextView) view.findViewById(R.id.item_description);
        this.itemFolio = (TextView) view.findViewById(R.id.item_folio);
        View editItem = view.findViewById(R.id.edit_item);
        editItem.setOnClickListener(this);
        this.listener = listener;
    }

    public void setupData(Item item){
        this.item = item;
        this.itemDescription.setText(item.getDescription());
        this.itemFolio.setText(item.getFolio());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_item:
                listener.onEditItemClick(item);
        }
    }
}
