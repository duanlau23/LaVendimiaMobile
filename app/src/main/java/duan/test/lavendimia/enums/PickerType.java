package duan.test.lavendimia.enums;

/**
 * Created by duan_ on 19/02/2017.
 */

public enum PickerType {
    SINGLE, MULTIPLE
}
