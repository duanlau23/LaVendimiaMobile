package duan.test.lavendimia.objects.observables;

import java.util.Observable;
import java.util.Observer;

import duan.test.lavendimia.listeners.ObservableClientListener;
import duan.test.lavendimia.objects.Client;

/**
 * Created by duan_ on 26/02/2017.
 */

public class ClientObserver implements Observer {

    private ObservableClient clientData;
    private ObservableClientListener mListener;

    public ClientObserver(ObservableClient data, ObservableClientListener listener){
        this.clientData = data;
        this.mListener = listener;
    }

    @Override
    public void update(Observable observable, Object data) {
        try{
            mListener.addItem((Client) data);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
