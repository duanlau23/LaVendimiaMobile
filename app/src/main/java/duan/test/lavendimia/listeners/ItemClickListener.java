package duan.test.lavendimia.listeners;

/**
 * Created by duan_ on 25/02/2017.
 */

public interface ItemClickListener {

    void onItemClick(Object item);

    void onEditItemClick(Object item);
}
