package duan.test.lavendimia.activities.splash_screen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.R;
import duan.test.lavendimia.activities.LoginActivity;
import duan.test.lavendimia.activities.MainActivity;
import duan.test.lavendimia.db.DbHelper;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.ThemeUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //checkStatus();
        showMain();
    }

    private void checkStatus() {
        IAsyncApiRequest request = new AsyncApiRequest(this.getApplicationContext());
        request.Post(TypeRequest.CHECK_STATUS, new JSONObject(), new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        DbHelper helper = DbHelper.getInstance(getApplicationContext());
                        helper.saveUserData(data);
                        checkStatusSuccess();
                    } else {
                        DbHelper helper = DbHelper.getInstance(getApplicationContext());
                        helper.clearSession();
                        checkStatusFailed();
                    }
                } catch (JSONException e) {
                    checkStatusFailed();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    checkStatusFailed();
                }
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    /*
                     DbHelper helper = DbHelper.getInstance(getApplicationContext());
                    helper.refreshUserData();
                    if (Utils.USER_ID != null) {
                        checkStatusSuccess();
                    } else {
                        checkStatusFailed();
                    }
                     */
                    connectionError();
                }
            }
        });
    }


    public void checkStatusFailed() {
        showLogin();
    }

    public void checkStatusSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private void showMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void connectionError(){
        new AlertDialog.Builder(this, R.style.AppThemeDarkDialog)
                .setIcon(R.drawable.ic_vn_alert)
                .setTitle(R.string.connection_error_title)
                .setMessage(R.string.connection_error_description)
                .setPositiveButton(R.string.retry_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restartApp();
                    }

                })
                .setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

    private void restartApp(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
