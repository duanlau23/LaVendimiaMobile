package duan.test.lavendimia.activities.splash_screen;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import java.util.concurrent.TimeUnit;

import duan.test.lavendimia.R;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.ThemeUtil;
import duan.test.lavendimia.utils.Utils;


public class LaVendimia extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(!prefs.contains(Utils.SHARED_THEME_NAME)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(Utils.SHARED_THEME_NAME, Utils.THEME_DARK);
            editor.apply();
        }

        setTheme(R.style.SplashScreenTheme);
        setupObjects();
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    }

    private void setupObjects(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(Utils.APLICATION_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        if(!prefs.contains(Names.FINANCING_RATE)){
            editor.putFloat(Names.FINANCING_RATE, 2.8f);
        }

        if(!prefs.contains(Names.HITCH)){
            editor.putFloat(Names.HITCH, 20.0f);
        }

        if(!prefs.contains(Names.DEADLINE)){
            editor.putInt(Names.DEADLINE, 12);
        }
        editor.apply();

        Utils.FINANCING_RATE = prefs.getFloat(Names.FINANCING_RATE, 0);
        Utils.HITCH =  prefs.getFloat(Names.HITCH, 0);
        Utils.DEADLINE = prefs.getInt(Names.DEADLINE, 0);
    }
}
