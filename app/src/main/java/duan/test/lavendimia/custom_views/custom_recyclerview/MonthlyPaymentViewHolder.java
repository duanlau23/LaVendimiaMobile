package duan.test.lavendimia.custom_views.custom_recyclerview;
;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.MonthlyPayment;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 04/12/2016.
 */
class MonthlyPaymentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView paymentCount;
    private TextView paymentAmmount;
    private TextView totalToPay;
    private TextView youSave;
    private RadioButton paymentSelected;
    private ItemClickListener listener;
    private MonthlyPayment monthlyPayment;

    MonthlyPaymentViewHolder(View view, ItemClickListener listener) {
        super(view);
        this.paymentCount = (TextView) view.findViewById(R.id.payment_count);
        this.paymentAmmount = (TextView) view.findViewById(R.id.payment_ammount);
        this.totalToPay = (TextView) view.findViewById(R.id.total_to_pay_view);
        this.youSave = (TextView) view.findViewById(R.id.you_save);
        this.paymentSelected = (RadioButton) view.findViewById(R.id.payment_selected);
        this.listener = listener;
        this.paymentSelected.setOnClickListener(this);
    }

    void setupData(MonthlyPayment monthlyPayment){
        this.monthlyPayment = monthlyPayment;
        this.paymentCount.setText(String.valueOf(monthlyPayment.getPaymentCount()));
        this.paymentAmmount.setText(String.valueOf(round(monthlyPayment.getPaymentAmmount(),2)));
        this.totalToPay.setText(String.valueOf(round(monthlyPayment.getPaymentTotal(),2)));
        this.youSave.setText(String.valueOf(round(monthlyPayment.getYouSaves(),2)));
        this.paymentSelected.setChecked(monthlyPayment.isSelected());
    }

    private static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        DecimalFormat df = new DecimalFormat("#.##");
        return Float.valueOf(df.format(value));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.payment_selected:
                listener.onEditItemClick(monthlyPayment);
        }
    }
}
