package duan.test.lavendimia.db;

/**
 * Created by digiteckno on 26/07/16.
 */
public interface DBActionListener {

    void onSucess();

    void onStart();

    void onError();

}
