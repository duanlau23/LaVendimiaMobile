package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.text.NumberFormat;

import duan.test.lavendimia.R;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;


public class SettingsFragment extends DialogFragment
        implements View.OnClickListener {

    View v;
    EditText financingRateView;
    EditText hitchView;
    EditText deadlineView;
    View saveButton;
    boolean financingHasError;
    boolean hitchHasError;
    boolean deadlineHasError;
    private boolean isInEditMode;
    private boolean clientIsSaved;
    ProgressDialog progressDialog;
    String folio;
    String regex = "[0-9]{1,3}(\\\\.[0-9]{2})?";
    String[] partialRegexChecks = {
            ".*[radio_unchequed-z]+.*", // lower
            ".*[A-Z]+.*", // upper
            ".*[0-9]+.*", // digits
            ".*[@#$%+\\-;\\*<>=.,]+.*" // symbols
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar actionBar = (Toolbar) v.findViewById(R.id.settings_toolbar);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
        if (actionBar != null) {
            final SettingsFragment window = this;
            actionBar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                }
            });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

    }

    public AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppThemeDark);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                if(clientIsSaved)
                    return true;

                if ((i ==  KeyEvent.KEYCODE_BACK) && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                    return false;
                }
                else
                    return false;
            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_settings, null);
        setupViews();
        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        financingRateView = (EditText) v.findViewById(R.id.financing_rate_view);
        hitchView = (EditText) v.findViewById(R.id.hitch_view);
        deadlineView = (EditText) v.findViewById(R.id.deadline_view);
        saveButton = v.findViewById(R.id.save_button);

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.saving_title_text));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);

        saveButton.setOnClickListener(this);
        financingRateView.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    financingRateView.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100)).replace("$","");

                    current = formatted;
                    financingRateView.setText(formatted);
                    financingRateView.setSelection(formatted.length());
                    financingRateView.addTextChangedListener(this);

                    if (Double.valueOf(financingRateView.getText().toString()) > 100){
                        financingRateView.setError(getResources().getString(R.string.decimal_format_error));
                        financingHasError = true;
                        return;
                    }

                    if (Double.valueOf(financingRateView.getText().toString()) == 0){
                        financingRateView.setError(getResources().getString(R.string.decimal_format_error));
                        financingHasError = true;
                        return;
                    }

                    financingHasError = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        hitchView.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    hitchView.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100)).replace("$","");

                    current = formatted;
                    hitchView.setText(formatted);
                    hitchView.setSelection(formatted.length());
                    hitchView.addTextChangedListener(this);

                    if (Double.valueOf(hitchView.getText().toString()) > 100){
                        hitchView.setError(getResources().getString(R.string.decimal_format_error));
                        hitchHasError = true;
                        return;
                    }

                    if (Double.valueOf(hitchView.getText().toString()) == 0){
                        hitchView.setError(getResources().getString(R.string.decimal_format_error));
                        hitchHasError = true;
                        return;
                    }

                    hitchHasError = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });


        setupData();
    }

    private boolean checkViews(){
        financingRateView.setText(financingRateView.getText() != null ? financingRateView.getText() : "");
        hitchView.setText(hitchView.getText() != null ? hitchView.getText() : "");
        deadlineView.setText(deadlineView.getText() != null ? deadlineView.getText() : "");

        if(financingRateView.getText().toString().equals("")){
            financingRateView.setError(getResources().getString(R.string.empty_financing_rate));
            financingHasError = true;
        }

        if(hitchView.getText().toString().equals("")){
            hitchView.setError(getResources().getString(R.string.empty_hitch));
            hitchHasError = true;
        }

        if(deadlineView.getText().toString().equals("")){
            deadlineView.setError(getResources().getString(R.string.empty_deadline));
            deadlineHasError = true;
        }

            return !financingHasError & !hitchHasError & !deadlineHasError;
    }

    private void saveClient(){
        if(checkViews()){
            SharedPreferences prefs = getContext().getSharedPreferences(Utils.APLICATION_ID, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putFloat(Names.FINANCING_RATE, Float.parseFloat((NumberFormat.getCurrencyInstance().format((Double.parseDouble(financingRateView.getText().toString().
                    replaceAll("[,.]", "")))/100)).replace("$", "")));
            editor.putFloat(Names.HITCH, Float.parseFloat((NumberFormat.getCurrencyInstance().format((Double.parseDouble(hitchView.getText().toString().
                    replaceAll("[,.]", "")))/100)).replace("$", "")));
            editor.putInt(Names.DEADLINE, Integer.valueOf(deadlineView.getText().toString()));
            editor.apply();

            Utils.FINANCING_RATE = prefs.getFloat(Names.FINANCING_RATE, 0);
            Utils.HITCH = prefs.getFloat(Names.HITCH, 0);
            Utils.DEADLINE = prefs.getInt(Names.DEADLINE, 0);

            onSaveGeoFenceSuccess();
        }
    }

    private void onSaveGeoFenceSuccess(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        Toast.makeText(getContext(),getResources().getString(R.string.save_changes_ok), Toast.LENGTH_SHORT).show();
        clientIsSaved = true;
        dismiss();
    }

    private void onSaveGeoFenceFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.save_client_error), Toast.LENGTH_LONG).show();
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        clientIsSaved = false;
    }


    private void setupData(){
        financingRateView.setText(String.valueOf(Utils.FINANCING_RATE * 10));
        hitchView.setText(String.valueOf(Utils.HITCH * 10));
        deadlineView.setText(String.valueOf(Utils.DEADLINE));
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.save_button){
            saveClient();
        }
    }
}

