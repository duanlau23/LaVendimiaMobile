package duan.test.lavendimia.objects.observables;

import java.util.Observable;
import java.util.Observer;

import duan.test.lavendimia.listeners.ObservableClientListener;
import duan.test.lavendimia.listeners.ObservableItemListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 26/02/2017.
 */

public class ItemObserver implements Observer {

    private ObservableItem itemData;
    private ObservableItemListener mListener;

    public ItemObserver(ObservableItem data, ObservableItemListener listener){
        this.itemData = data;
        this.mListener = listener;
    }

    @Override
    public void update(Observable observable, Object data) {
        try{
            mListener.addItem((Item) data);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
