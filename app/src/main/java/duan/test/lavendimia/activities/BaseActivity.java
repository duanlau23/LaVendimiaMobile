package duan.test.lavendimia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.SettingType;
import duan.test.lavendimia.listeners.SettingsChangeListener;
import duan.test.lavendimia.utils.ThemeUtil;
import duan.test.lavendimia.utils.Utils;


/**
 * Created by duan_ on 28/01/2017.
 */

public class BaseActivity extends AppCompatActivity implements SettingsChangeListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateTheme();
    }
    public void updateTheme() {
        setTheme(R.style.AppThemeDark);
    }

    @Override
    public void onSettingChange(SettingType settingType) {
        switch (settingType){
            case THEME:
                reCreateActivity();
        }
    }

    private void reCreateActivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
