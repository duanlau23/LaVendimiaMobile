package duan.test.lavendimia.utils;

/**
 * Created by duan_ on 26/10/2016.
 */

public class Names {
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String LAST_NAME = "lastName";
    public static final String MA_LAST_NAME = "maLastName";
    public static final String RFC = "rfc";
    public static final String FOLIO = "folio";
    public static final String IS_VALID = "isValid";
    public static final String HAS_MESSAGES = "hasMessages";
    public static final String MESSAGE = "message";
    public static final String VALUE = "value";
    public static final String DATA = "data";
    public static final String DESCRIPTION = "description";
    public static final String MODEL = "model";
    public static final String PRICE = "price";
    public static final String EXISTENCE = "existence";
    public static final String HITCH = "hitch";
    public static final String DEADLINE = "deadline";
    public static final String FINANCING_RATE = "financingRate";
    public static final String HITCH_BONIFICATION = "hitchBonification";
    public static final String CLIENT = "client";
    public static final String ITEMS = "items";
    public static final String TOTAL = "total";
    public static final String PAYMENT_COUNT = "paymentCount";


    //default names
    public static final String APLICATION_ID = "app_id";
    public static final String TOKEN = "token";

}
