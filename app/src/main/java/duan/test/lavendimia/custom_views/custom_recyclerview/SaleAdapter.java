package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 04/12/2016.
 */

public class SaleAdapter extends RecyclerView.Adapter<SaleViewHolder> {
    private List<Sale> saleList;
    private ItemClickListener listener;

    public SaleAdapter(List<Sale> saleList, ItemClickListener listener){
        this.saleList = saleList;
        this.listener = listener;
    }

    @Override
    public SaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_item, parent, false);
        return new SaleViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final SaleViewHolder holder, int position) {

        Sale sale = getItem(position);

        if(sale == null){
            return;
        }

        holder.setupData(sale);

    }


    @Override
    public int getItemCount() {
        return saleList.size();
    }

    private Sale getItem(int i){
        try{
            return saleList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
