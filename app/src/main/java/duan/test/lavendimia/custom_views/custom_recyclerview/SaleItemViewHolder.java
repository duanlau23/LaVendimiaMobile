package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 04/12/2016.
 */
 public  class SaleItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView itemDescriptionView;
    private TextView itemFolioView;
    private EditText quantityView;
    private TextView itemModelView;
    private TextView itemPriceView;
    private TextView itemAmmountView;
    private ItemClickListener listener;
    private View deleteItemView;
    private Item item;
    private Context context;

    SaleItemViewHolder(View view, ItemClickListener lis, final Context context) {
        super(view);
        this.context = context;
        this.itemDescriptionView = (TextView) view.findViewById(R.id.item_description);
        this.itemFolioView = (TextView) view.findViewById(R.id.item_folio);
        this.quantityView = (EditText) view.findViewById(R.id.quantity_view);
        this.itemModelView = (TextView) view.findViewById(R.id.model_view);
        this.itemPriceView = (TextView) view.findViewById(R.id.item_price);
        this.itemAmmountView = (TextView) view.findViewById(R.id.item_ammount);
        this.deleteItemView = view.findViewById(R.id.delete_item);
        this.deleteItemView.setOnClickListener(this);
        this.quantityView.setOnClickListener(this);
        this.listener = lis;
    }

    public void setupData(final Item tempItem){
        this.item = tempItem;
        this.itemDescriptionView.setText(item.getDescription());
        this.itemFolioView.setText(item.getFolio());
        this.itemModelView.setText(item.getModel() != null ? item.getModel() : "");
        this.itemPriceView.setText(String.valueOf(item.getPublicPrice()));
        this.itemAmmountView.setText(String.valueOf(item.getPublicPrice() * item.getQuantity()));
        this.quantityView.setText(String.valueOf(item.getQuantity()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.delete_item:
                listener.onEditItemClick(item);
                break;
            case R.id.quantity_view:
                selectNumber();
                break;

        }
    }

    private void selectNumber(){
        final Dialog d = new Dialog(context, R.style.AppThemeDarkDialog);
        d.setTitle("Selecciona cantidad");
        d.setContentView(R.layout.number_picker_dialog);
        View acceptButton = d.findViewById(R.id.accept_button);
        View cancelButton = d.findViewById(R.id.cancel_button);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.number_picker);
        np.setMaxValue(item.getExistence());
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        acceptButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                quantityView.setText(String.valueOf(np.getValue()));
                item.setQuantity(np.getValue());
                listener.onItemClick(item);
                d.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

}
