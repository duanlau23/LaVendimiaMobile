package duan.test.lavendimia.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.R;
import duan.test.lavendimia.db.DbHelper;
import duan.test.lavendimia.enums.FragmentAction;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.fragments.ForgotPasswordFragment;
import duan.test.lavendimia.fragments.SignUpFragment;
import duan.test.lavendimia.listeners.CheckStatusListener;
import duan.test.lavendimia.listeners.FragmentActionListener;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Utils;

public class LoginActivity extends AppCompatActivity implements FragmentActionListener{


    EditText mEmailUserTextView;
    EditText mPasswordTextView;
    View mSignInButton;
    View mSignInContainer;
    View mSignUpContainer;
    Button mForgotPasswordButton;
    boolean checkStatusEnd;
    boolean checkStatusSuccess;
    private boolean userHasError = true;
    private boolean passwordHasError = true;
    CheckStatusListener checkListener;
    ForgotPasswordFragment forgotPasswordFragment;
    SignUpFragment mSignUpFragment;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_login);

        setupViews();
        setupAnimations();

        checkListener = new CheckStatusListener() {
            @Override
            public void onCheckStatusSuccess() {
                checkStatusSuccess = true;
                checkStatusSuccess();
                checkStatusEnd = true;
            }

            @Override
            public void onCheckStatusFailed() {
                checkStatusSuccess = false;
                checkStatusFailed();
                checkStatusEnd = true;
            }

            @Override
            public void onCheckStatusRepeat() {

            }
        };
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        setupViews();
    }

    private void setupAnimations() {

    }


    private void setupViews() {
        mEmailUserTextView = (EditText) findViewById(R.id.user_or_email);
        mPasswordTextView = (EditText) findViewById(R.id.password);
        mSignInButton = findViewById(R.id.sign_in);
        mForgotPasswordButton = (Button) findViewById(R.id.forgot_password_button);
        mSignInContainer = findViewById(R.id.sign_in_container);
        mSignUpContainer = findViewById(R.id.sign_up_container);

        mEmailUserTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mEmailUserTextView.length() == 0) {
                    mEmailUserTextView.setError(null);
                    userHasError = true;
                    return;
                }

                if (mEmailUserTextView.length() < getResources().getInteger(R.integer.user_min_length)) {
                    mEmailUserTextView.setError(getResources().getString(R.string.contains_characters));
                    userHasError = true;
                    return;
                }

                userHasError = false;
            }
        });

        mPasswordTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (mPasswordTextView.length() == 0) {
                    mPasswordTextView.setError(null);
                    passwordHasError = true;
                    return;
                }

                if (mPasswordTextView.length() < getResources().getInteger(R.integer.password_min_length)) {
                    mPasswordTextView.setError(getResources().getString(R.string.contains_8characters));
                    passwordHasError = true;
                    return;
                }

                passwordHasError = false;
            }
        });

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkViews()){
                    logIn();
                }
            }
        });

        mSignUpContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignUp();
            }
        });


        mForgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForgotPassword();
            }
        });



    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("user", mEmailUserTextView.getText().toString());
        outState.putString("password", mPasswordTextView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState == null)
            return;

        mEmailUserTextView.setText(savedInstanceState.containsKey("user") ? savedInstanceState.getString("user") : "");
        mPasswordTextView.setText(savedInstanceState.containsKey("password") ? savedInstanceState.getString("password") : "");
    }



    private boolean checkViews() {

        if (mEmailUserTextView.length() == 0) {
            mEmailUserTextView.setError(getResources().getString(R.string.empty_user));
            userHasError = true;
        }


        if (mEmailUserTextView.length() < getResources().getInteger(R.integer.user_min_length) && mEmailUserTextView.length() > 0) {
            mEmailUserTextView.setError(getResources().getString(R.string.contains_characters));
            userHasError = true;
        }


        if (mPasswordTextView.length() == 0) {
            mPasswordTextView.setError(getResources().getString(R.string.empty_password));
            passwordHasError = true;
        }

        if (mPasswordTextView.length() < getResources().getInteger(R.integer.password_min_length) && mPasswordTextView.length() > 0) {
            mPasswordTextView.setError(getResources().getString(R.string.contains_8characters));
            passwordHasError = true;
        }

        return !(userHasError || passwordHasError);
    }

    private void showForgotPassword() {
        if (forgotPasswordFragment == null) {
            forgotPasswordFragment = new ForgotPasswordFragment();
        }
        if (forgotPasswordFragment.isVisible())
            return;

        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putInt("width", mSignInContainer.getMeasuredWidth());
        bundle.putInt("height", mSignInContainer.getMeasuredHeight());
        forgotPasswordFragment = new ForgotPasswordFragment();
        forgotPasswordFragment.setArguments(bundle);
        forgotPasswordFragment.show(fm, "login_fragment");
    }

    private void showFeed() {
        /*if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        Intent i = new Intent(getContext(), FeedActivity.class);
        startActivity(i);*/
        progressDialog.dismiss();

    }


    public void logIn() {
        progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getResources().getString(R.string.sign_in_text));
        progressDialog.show();

        String user = mEmailUserTextView.getText().toString();
        String pass = mPasswordTextView.getText().toString();

        JSONObject data = new JSONObject();
        try {
            data.put("username", user);
            data.put("pass", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        onLoginSuccess(data);
    }

    private void showSignUp() {
        if (mSignUpFragment == null) {
            mSignUpFragment = new SignUpFragment();
        }
        if (mSignUpFragment.isVisible())
            return;

        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putInt("width", mSignInContainer.getMeasuredWidth());
        bundle.putInt("height", mSignInContainer.getMeasuredHeight());
        mSignUpFragment = new SignUpFragment();
        mSignUpFragment.setArguments(bundle);
        mSignUpFragment.show(fm, "login_fragment");
    }


    public void onLoginSuccess(JSONObject data) {

        IAsyncApiRequest request = new AsyncApiRequest(getApplicationContext());
        request.Post(TypeRequest.SIGN_IN, data, new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        DbHelper helper = DbHelper.getInstance(getApplicationContext());
                        helper.saveToken(data);
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();
                        showMain();
                    } else {
                        onLoginFailed();
                    }
                } catch (JSONException e) {
                    onLoginFailed();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                onLoginFailed();
            }

            @Override
            public void onRetry(int number) {
            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        });
    }

    public void onLoginFailed() {
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }


    private void checkStatus(final CheckStatusListener checkListener) {

        progressDialog = new ProgressDialog(getApplicationContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);


        IAsyncApiRequest request = new AsyncApiRequest(getApplicationContext());
        request.Post(TypeRequest.CHECK_STATUS, new JSONObject(), new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        DbHelper helper = DbHelper.getInstance(getApplicationContext());
                        helper.saveUserData(data);
                        checkListener.onCheckStatusSuccess();
                    } else {
                        checkListener.onCheckStatusFailed();
                    }
                } catch (JSONException e) {
                    checkListener.onCheckStatusFailed();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    checkListener.onCheckStatusFailed();
                }
            }

            @Override
            public void onRetry(int number) {
                checkListener.onCheckStatusRepeat();
            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                DbHelper helper = DbHelper.getInstance(getApplicationContext());
                helper.refreshUserData();
                if (Utils.USER_ID != null) {
                    checkListener.onCheckStatusSuccess();
                } else {
                    checkListener.onCheckStatusFailed();
                }
            }
        });
    }

    public void checkStatusFailed() {
        progressDialog.dismiss();
    }

    public void checkStatusSuccess() {
        if(progressDialog.isShowing())
            progressDialog.dismiss();
        showMain();
    }

    private void showMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFragmentAction(FragmentAction action) {
        switch (action) {
            case SIGN_UP_SUCCESS:
                showMain();
                break;
            case LOG_IN:
                showMain();
                break;
        }
    }
}
