package duan.test.lavendimia.objects;

import duan.test.lavendimia.utils.Utils;

/**
 * Created by duan_ on 28/02/2017.
 */

public class MonthlyPayment {

    private int paymentCount;
    private float paymentAmmount;
    private float paymentTotal;
    private float youSaves;
    private boolean selected;

    public MonthlyPayment(){}

    public MonthlyPayment(float total,float countedPrice, int limit){
        this.paymentCount = limit;
        this.paymentTotal = countedPrice*(1+(Utils.FINANCING_RATE * limit)/100);
        this.paymentAmmount = this.paymentTotal/limit;
        this.youSaves = total - this.paymentTotal;
    }

    public void updatePayment(float total, float countedPrice){
        this.paymentTotal = countedPrice *( 1+(Utils.FINANCING_RATE * this.paymentCount)/100);
        this.paymentAmmount = this.paymentTotal/this.paymentCount;
        this.youSaves =total - this.paymentTotal;
    }

    public int getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(int paymentCount) {
        this.paymentCount = paymentCount;
    }

    public float getPaymentAmmount() {
        return paymentAmmount;
    }

    public void setPaymentAmmount(float paymentAmmount) {
        this.paymentAmmount = paymentAmmount;
    }

    public float getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(float paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public float getYouSaves() {
        return youSaves;
    }

    public void setYouSaves(float youSaves) {
        this.youSaves = youSaves;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
