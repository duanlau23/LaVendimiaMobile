package duan.test.lavendimia.enums;

/**
 * Created by digiteckno on 06/07/16.
 */
public enum ConnectionType {
    CONNECTED, DISCONNECTED
}
