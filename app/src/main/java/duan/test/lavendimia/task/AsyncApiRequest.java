package duan.test.lavendimia.task;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import duan.test.lavendimia.db.DbHelper;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by digiteckno on 18/04/16.
 */
public class AsyncApiRequest implements IAsyncApiRequest {


    private static AsyncHttpClient client;
    private boolean result = false;
    private Context _context;
    private List<String> failureItems = new ArrayList<>();
    private boolean isGettingData = false;

    public AsyncApiRequest(Context context) {
        client = new AsyncHttpClient();
        _context = context;

    }

    @Override
    public boolean Put(final TypeRequest request, JSONObject data, final ApiResponse respuesta) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        boolean connection = isNetworkAvailable();

        respuesta.onConnection(connection);
        if (!connection)
            return false;

        if (request != TypeRequest.SIGN_IN) {
            DbHelper helper = DbHelper.getInstance(_context);
            String token = helper.readKey();

            if (token == null) {
                token = "";
            }

            client.addHeader(Utils.X_SESSION_HEADER, token);
            client.addHeader(Utils.X_SESSION_REMEMBER_HEADER, "true");
        }

        if (request != TypeRequest.CHECK_STATUS) {
            params = new RequestParams();
            params.put(Names.VALUE, setupRequestParams(request, data));
            client.setMaxRetriesAndTimeout(4, 2000);
        }
        client.addHeader(Utils.X_APP_ID_HEADER, Utils.APLICATION_ID);


        try {
            client.put(request.getURL(), params, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"
                            JSONObject _res = null;
                            try {
                                if (res != null) {
                                    _res = new JSONObject(res);
                                    if (!_res.getBoolean(Names.IS_VALID) && _res.getBoolean(Names.HAS_MESSAGES)) {
                                        if (request != TypeRequest.CHECK_STATUS) {
                                            Toast.makeText(_context.getApplicationContext(), _res.getJSONObject(Names.MESSAGE).getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    respuesta.onSuccess(_res);
                                } else {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(_res);
                                }

                            } catch (JSONException e) {
                                try {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(new JSONObject(res));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                            }
                            result = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            int a = 0;
                            JSONObject _res = null;
                            if (statusCode != 0) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                try {
                                    if (res != null) {
                                        _res = new JSONObject(res);
                                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                        respuesta.onError(_res);
                                    }
                                } catch (JSONException e) {
                                    try {
                                        respuesta.onError(new JSONObject(res));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                            } else {
                                ConnectivityManager connectivityManager = (ConnectivityManager) _context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if (activeNetworkInfo == null) {
                                    //Toast.makeText(_context.getApplicationContext(), _context.getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                                    try {
                                        _res = new JSONObject();
                                        _res.put(Names.IS_VALID, false);
                                        respuesta.onError(_res);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.d("retry", "no: " + retryNo);
                            respuesta.onRetry(retryNo);
                            super.onRetry(retryNo);
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                        }


                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean Post(final TypeRequest request, JSONObject data, final ApiResponse respuesta) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        boolean connection = isNetworkAvailable();


        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySSLSocketFactory socketFactory = new MySSLSocketFactory(trustStore);
            socketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            client.setSSLSocketFactory(socketFactory);
        } catch (KeyStoreException | KeyManagementException | CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | IOException e) {
            e.printStackTrace();
        }

        respuesta.onConnection(connection);
        if (!connection)
            return false;

        if (request != TypeRequest.SIGN_IN) {
            DbHelper helper = DbHelper.getInstance(_context);
            String token = helper.readKey();

            if (token == null) {
                token = "";
            }

            client.addHeader(Utils.X_SESSION_HEADER, token);
            client.addHeader(Utils.X_SESSION_REMEMBER_HEADER, "true");
        }
        if (request != TypeRequest.CHECK_STATUS) {
            params = new RequestParams();
            params.put(Names.VALUE, setupRequestParams(request, data));
        }

        client.setMaxRetriesAndTimeout(4, 2000);
        client.addHeader(Utils.X_APP_ID_HEADER, Utils.APLICATION_ID);
        //client.addHeader("origin", "android");


        try {
            client.post(request.getURL(), params, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"

                            JSONObject _res = null;
                            try {
                                if (res != null) {
                                    _res = new JSONObject(res);
                                    if (!_res.getBoolean(Names.IS_VALID)) {
                                        if(_res.has(Names.HAS_MESSAGES)){
                                            if(_res.getBoolean(Names.HAS_MESSAGES)){
                                                if (request != TypeRequest.CHECK_STATUS) {
                                                    Toast.makeText(_context.getApplicationContext(), _res.getJSONObject(Names.MESSAGE).getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                    respuesta.onSuccess(_res);
                                } else {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(_res);
                                }

                            } catch (JSONException e) {
                                try {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(new JSONObject(res));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                            }
                            result = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            int a = 0;
                            JSONObject _res = null;
                            if (statusCode != 0) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                try {
                                    if (res != null) {
                                        _res = new JSONObject(res);
                                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                        respuesta.onError(_res);
                                    }
                                } catch (JSONException e) {
                                    try {
                                        respuesta.onError(new JSONObject(res));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                            } else {
                                ConnectivityManager connectivityManager = (ConnectivityManager) _context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if (activeNetworkInfo == null) {
                                    //Toast.makeText(_context.getApplicationContext(), _context.getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                                    try {
                                        _res = new JSONObject();
                                        _res.put(Names.IS_VALID, false);
                                        respuesta.onError(_res);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.d("retry", "no: " + retryNo);
                            /*Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " " +
                                    _context.getResources().getString(R.string.retry_no) + " " + retryNo + ")", Toast.LENGTH_LONG).show();*/
                            respuesta.onRetry(retryNo);
                            super.onRetry(retryNo);
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean Delete(final TypeRequest request, JSONObject data, final ApiResponse respuesta) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        boolean connection = isNetworkAvailable();

        respuesta.onConnection(connection);
        if (!connection)
            return false;

        if (request != TypeRequest.SIGN_IN && request != TypeRequest.SIGN_UP) {
            DbHelper helper = DbHelper.getInstance(_context);
            String token = helper.readKey();

            if (token == null) {
                token = "";
            }

            client.addHeader(Utils.X_SESSION_HEADER, token);
            client.addHeader(Utils.X_SESSION_REMEMBER_HEADER, "true");
        }
        if (request != TypeRequest.CHECK_STATUS) {
            params = new RequestParams();
            params.put("value", setupRequestParams(request, data));
            client.setMaxRetriesAndTimeout(4, 2000);
        }
        client.addHeader(Utils.X_APP_ID_HEADER, Utils.APLICATION_ID);


        try {
            client.delete(request.getURL(), params, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"
                            JSONObject _res = null;
                            try {
                                if (res != null) {
                                    _res = new JSONObject(res);
                                    if (!_res.getBoolean(Names.IS_VALID) && _res.getBoolean(Names.HAS_MESSAGES)) {
                                        //  Toast.makeText(_context.getApplicationContext(), _res.getJSONObject("message").getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                                    }
                                    respuesta.onSuccess(_res);
                                } else {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(_res);
                                }

                            } catch (JSONException e) {
                                try {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(new JSONObject(res));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                            }
                            result = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            int a = 0;
                            JSONObject _res = null;
                            if (statusCode != 0) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                try {
                                    if (res != null) {
                                        _res = new JSONObject(res);
                                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                        respuesta.onError(_res);
                                    }
                                } catch (JSONException e) {
                                    try {
                                        respuesta.onError(new JSONObject(res));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                            } else {
                                ConnectivityManager connectivityManager = (ConnectivityManager) _context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if (activeNetworkInfo == null) {
                                    //Toast.makeText(_context.getApplicationContext(), _context.getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                                    try {
                                        _res = new JSONObject();
                                        _res.put(Names.IS_VALID, false);
                                        respuesta.onError(_res);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.d("retry", "no: " + retryNo);
                            //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " " +
                            //      _context.getResources().getString(R.string.retry_no) + " " + retryNo + ")", Toast.LENGTH_LONG).show();
                            respuesta.onRetry(retryNo);
                            super.onRetry(retryNo);
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean Get(final TypeRequest request, String params, final ApiResponse respuesta) {
        if (isGettingData) {
            JSONObject res = new JSONObject();
            try {
                res.put("isValid", false);
                res.put("hasMessages", false);
                respuesta.onError(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        boolean connection = isNetworkAvailable();

        respuesta.onConnection(connection);
        if (!connection)
            return false;

        isGettingData = true;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);

        client.addHeader(Utils.X_APP_ID_HEADER, Utils.APLICATION_ID);

        if(Utils.TOKEN != null){
            client.addHeader(Utils.X_SESSION_HEADER, Utils.TOKEN);
        }

        Log.d("test", request.getURL());

        try {
            client.setMaxRetriesAndTimeout(4, 1000);
             client.get(request.getURL(), new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"

                            JSONObject _res = null;
                            try {
                                if (res != null) {
                                    _res = new JSONObject(res);
                                    if (_res.getBoolean("isValid") && _res.getBoolean("hasMessages")) {
                                        //  Toast.makeText(_context.getApplicationContext(), _res.getJSONObject("message").getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                                    }
                                    respuesta.onSuccess(_res);
                                } else {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(_res);
                                }

                            } catch (JSONException e) {
                                try {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(new JSONObject(res));
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                                e.printStackTrace();
                            }


                            result = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            int a = 0;
                            JSONObject _res = null;
                            if (statusCode != 0) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                try {
                                    if (res != null) {
                                        _res = new JSONObject(res);
                                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                        respuesta.onError(_res);
                                    }
                                } catch (JSONException e) {
                                    try {
                                        JSONObject error = new JSONObject();
                                        error.put("isValid", false);
                                        error.put("hasMessages", true);
                                        error.put("message", res);
                                        respuesta.onError(error);
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.d("retry", "no: " + retryNo);
                            //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Reintento no. " + retryNo + ")", Toast.LENGTH_LONG).show();
                            respuesta.onRetry(retryNo);
                            super.onRetry(retryNo);
                        }

                        @Override
                        public void onFinish() {
                            isGettingData = false;
                            super.onFinish();
                        }

                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void GetSync(final TypeRequest request, String params, final ApiResponse respuesta) {


        if (isGettingData) {
            JSONObject res = new JSONObject();
            try {
                res.put("isValid", false);
                res.put("hasMessages", false);
                respuesta.onError(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return;
        }

        boolean connection = isNetworkAvailable();

        respuesta.onConnection(connection);
        if (!connection)
            return;

        isGettingData = true;
        AsyncHttpClient client = new AsyncHttpClient();

        Looper looper = Looper.getMainLooper();
        if (Looper.myLooper() == null)
        {
            Looper.prepare();
        }
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler(looper) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String res = new String(responseBody);
                // called when response HTTP status is "200 OK"
                JSONObject _res = null;
                try {
                    if (res != null) {
                        _res = new JSONObject(res);
                        if (_res.getBoolean("isValid") && _res.getBoolean("hasMessages")) {
                            //  Toast.makeText(_context.getApplicationContext(), _res.getJSONObject("message").getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                        }
                        respuesta.onSuccess(_res);
                    } else {
                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                        respuesta.onError(_res);
                    }

                } catch (JSONException e) {
                    try {
                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                        respuesta.onError(new JSONObject(res));
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    e.printStackTrace();
                }


                result = true;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                int a = 0;
                String res = "";
                if (responseBody != null) {
                    res = new String(responseBody);
                }

                JSONObject _res = null;
                if (statusCode != 0) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    try {
                        if (res != null) {
                            _res = new JSONObject(res);
                            //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                            respuesta.onError(_res);
                        }
                    } catch (JSONException e) {
                        try {
                            respuesta.onError(new JSONObject(res));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onRetry(int retryNo) {
                Log.d("retry", "no: " + retryNo);
                //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Reintento no. " + retryNo + ")", Toast.LENGTH_LONG).show();
                respuesta.onRetry(retryNo);
                super.onRetry(retryNo);
            }

            @Override
            public void onFinish() {
                isGettingData = false;
                super.onFinish();
            }
        };


        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
            client.addHeader(Utils.X_APP_ID_HEADER, preferences.getString(Names.APLICATION_ID,""));
            client.addHeader(Utils.X_SESSION_HEADER, preferences.getString(Names.TOKEN,""));
            client.addHeader(Utils.X_SESSION_REMEMBER_HEADER, "true");
            client.get(request.getURL(), handler
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean UploadImage(final TypeRequest request, final String imageUrl, final ApiResponse respuesta) {

        boolean connection = isNetworkAvailable();

        respuesta.onConnection(connection);
        if (!connection)
            return false;

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        MimeTypeMap map = MimeTypeMap.getSingleton();
        File image = new File(imageUrl);
        String ext = MimeTypeMap.getFileExtensionFromUrl(image.toURI().toString());
        String mimeType = map.getMimeTypeFromExtension(ext);
        try {
            client.addHeader(Utils.X_APP_ID_HEADER, Utils.APLICATION_ID);
            params.put("images", image, mimeType);
            params.setHttpEntityIsRepeatable(true);
            params.setUseJsonStreamer(false);
            client.setMaxRetriesAndTimeout(1, 1000);
            client.post(request.getURL(), params, new TextHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String res) {
                            // called when response HTTP status is "200 OK"

                            JSONObject _res = null;
                            try {
                                if (res != null) {
                                    _res = new JSONObject(res);
                                    if (!_res.getBoolean("isValid") && _res.getBoolean("hasMessages")) {
                                        // Toast.makeText(_context.getApplicationContext(), _res.getJSONObject("message").getString(Utils.LANGUAGE.getAbb()), Toast.LENGTH_LONG).show();
                                    }
                                    respuesta.onSuccess(_res);
                                } else {
                                    //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                    respuesta.onError(_res);
                                }

                            } catch (JSONException e) {
                                //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                respuesta.onError(_res);
                                e.printStackTrace();
                            }
                            result = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                            int a = 0;
                            JSONObject _res = null;
                            if (statusCode != 0) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                try {
                                    if (res != null) {
                                        _res = new JSONObject(res);
                                        //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Error: " + statusCode + ")", Toast.LENGTH_LONG).show();
                                        respuesta.onError(_res);
                                    }
                                } catch (JSONException e) {
                                    respuesta.onError(_res);
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            Log.d("retry", "no: " + retryNo);
                            //Toast.makeText(_context.getApplicationContext(), request.getErrorText(_context.getResources()) + " (Reintento no. " + retryNo + ")", Toast.LENGTH_LONG).show();
                            respuesta.onRetry(retryNo);
                            super.onRetry(retryNo);
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                        }

                        @Override
                        public void onProgress(long bytesWritten, long totalSize) {
                            super.onProgress(bytesWritten, totalSize);
                            respuesta.onProgress((int) ((totalSize > 0) ? (bytesWritten * 1.0 / totalSize) * 100 : -1));
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*private boolean checkRequestItems(ApiResponse response){
        int c = 0;
        for (PostFile item: publicationItems) {
            if(item.isLoaded() == false){
                return false;
            }else if (item.getResponse() == StatusTypes.ERROR){
                failureItems.add(item.getUrl());
                if(c == publicationItems.size()-1){
                    response.onError(createFailureItemsJson());
                }
                return false;
            }
            c++;
        }
        return  true;
    }

    private JSONObject createSuccessedItemsJson(){
        JSONObject res = new JSONObject();
        JSONArray successedItems = new JSONArray();
        try {
            for (PostFile url:publicationItems) {
                JSONObject sucessedItem = new JSONObject();
                sucessedItem.put("url", url);
                successedItems.put(sucessedItem);
            }
            res.put("failItems", successedItems);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }
*/
    private JSONObject createFailureItemsJson() {
        JSONObject res = new JSONObject();
        JSONArray failItems = new JSONArray();
        try {
            for (String url : failureItems) {
                JSONObject failItem = new JSONObject();
                failItem.put("url", url);
                failItems.put(failItem);
            }
            res.put("failItems", failItems);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public File createTempFile(String namePart, int byteSize) {
        try {
            File f = File.createTempFile(namePart, "_handled", _context.getCacheDir());
            FileOutputStream fos = new FileOutputStream(f);
            Random r = new Random();
            byte[] buffer = new byte[byteSize];
            r.nextBytes(buffer);
            fos.write(buffer);
            fos.flush();
            fos.close();
            return f;
        } catch (Throwable t) {
            Log.e("Promtar", "createTempFile failed", t);
        }
        return null;
    }


    private String setupRequestParams(TypeRequest request, JSONObject data) {
        String result = "";
        switch (request) {
            default:
                result = Base64.encodeToString(data.toString().getBytes(), Base64.DEFAULT);
                break;
        }

        return result.replace("\n", "");
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();


        Log.d("connection", String.valueOf(activeNetworkInfo != null && activeNetworkInfo.isConnected()));
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
