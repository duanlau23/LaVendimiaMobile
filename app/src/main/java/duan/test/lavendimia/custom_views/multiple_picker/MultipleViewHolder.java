package duan.test.lavendimia.custom_views.multiple_picker;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.PickerType;
import duan.test.lavendimia.listeners.MultiplePickerListener;
import duan.test.lavendimia.objects.MultipleObject;

/**
 * Created by duan_ on 04/12/2016.
 */

class MultipleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView itemName;
    private CheckBox selectedItem;
    private MultiplePickerListener listener;
    private MultipleObject object;
    private PickerType type;

    MultipleViewHolder(View view, MultiplePickerListener listener, PickerType type) {
        super(view);
        this.listener = listener;
        this.type = type;
        this.itemName = (TextView) view.findViewById(R.id.item_name);
        this.selectedItem = (CheckBox) view.findViewById(R.id.selected_item);
        if (type == PickerType.SINGLE)
            selectedItem.setVisibility(View.GONE);
        view.setOnClickListener(this);
    }

    public void setupViews(final MultipleObject object){
        this.object = object;
        itemName.setText(object != null ? object.getName():"");
        if (object != null) {
            selectedItem.setChecked(object.isSelected());
        }
    }

    @Override
    public void onClick(View view) {
        if(type == PickerType.MULTIPLE){
            if(!object.isSelected()){
                object.setSelected(true);
                listener.onSelect(object);

            }else{
                object.setSelected(false);
                listener.onUnselect(object);
            }

            selectedItem.setChecked(object.isSelected());
        }else{
            listener.onSelect(object);
        }
    }
}
