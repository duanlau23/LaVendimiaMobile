package duan.test.lavendimia.objects;

import org.json.JSONObject;

/**
 * Created by duan_ on 18/02/2017.
 */

public class MultipleObject {

    private String name;
    private boolean isSelected;
    private JSONObject object;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getObject() {
        return object;
    }

    public void setObject(JSONObject object) {
        this.object = object;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
