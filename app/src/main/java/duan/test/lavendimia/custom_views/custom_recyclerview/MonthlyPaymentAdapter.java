package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.MonthlyPayment;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 04/12/2016.
 */

public class MonthlyPaymentAdapter extends RecyclerView.Adapter<MonthlyPaymentViewHolder> {
    private List<MonthlyPayment> monthlyPaymentList;
    private ItemClickListener listener;

    public MonthlyPaymentAdapter(List<MonthlyPayment> monthlyPaymentList, ItemClickListener listener){
        this.monthlyPaymentList = monthlyPaymentList;
        this.listener = listener;
    }

    @Override
    public MonthlyPaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_payment_item, parent, false);
        return new MonthlyPaymentViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final MonthlyPaymentViewHolder holder, int position) {

        MonthlyPayment sale = getItem(position);

        if(sale == null){
            return;
        }

        holder.setupData(sale);

    }


    @Override
    public int getItemCount() {
        return monthlyPaymentList.size();
    }

    private MonthlyPayment getItem(int i){
        try{
            return monthlyPaymentList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
