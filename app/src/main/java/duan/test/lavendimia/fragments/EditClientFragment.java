package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;


public class EditClientFragment extends DialogFragment
        implements View.OnClickListener {

    View v;
    Client client;
    EditText nameView;
    EditText lastNameView;
    EditText maLastNameView;
    EditText rfcView;
    TextView fragmentTitle;
    TextView folioView;
    View saveButton;


    private boolean nameHasError;
    private boolean lastNameHasError;
    private boolean maLastNameHasError;
    private boolean rfcHasError;
    private boolean isInEditMode;
    private boolean clientIsSaved;
    ProgressDialog progressDialog;
    private static String folio;
    private static String rfcRegex = "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\\d]{3})?$";
    private static String[] partialRegexChecks = {
            ".*[a-z]+.*", // lower
            ".*[A-Z]+.*", // upper
            ".*[0-9]+.*", // digits
            ".*[@#$%+\\-;\\*<>=.,]+.*" // symbols
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() == null)
            return;

        client = getArguments().getParcelable("sale");
        isInEditMode = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar actionBar = (Toolbar) v.findViewById(R.id.settings_toolbar);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
        if (actionBar != null) {
            final EditClientFragment window = this;
            actionBar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                }
            });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

    }

    public AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppThemeDark);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                if(clientIsSaved)
                    return true;

                if ((i ==  android.view.KeyEvent.KEYCODE_BACK) && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                    return false;
                }
                else
                    return false;
            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_edit_client, null);
        setupViews();
        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        nameView = (EditText) v.findViewById(R.id.name_view);
        lastNameView = (EditText) v.findViewById(R.id.last_name_view);
        maLastNameView = (EditText) v.findViewById(R.id.ma_last_name_view);
        rfcView = (EditText) v.findViewById(R.id.rfc_view);
        fragmentTitle = (TextView) v.findViewById(R.id.fragment_title);
        saveButton = v.findViewById(R.id.save_button);
        folioView = (TextView) v.findViewById(R.id.folio_view);

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.saving_title_text));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);

        if(client != null){
            folio = client.getFolio();
        }else{
            folio = "0001";
            if(Utils.OBSERVABLE_CLIENT_DATA != null){
                if(Utils.OBSERVABLE_CLIENT_DATA.getData().size() < 9999){
                    folio = folio.substring(0, (folio.length())-String.valueOf(Utils.OBSERVABLE_CLIENT_DATA.getData().size()).length()) + String.valueOf(Utils.OBSERVABLE_CLIENT_DATA.getData().size()+1);
                }
            }
        }



        folioView.setText(folio);

        saveButton.setOnClickListener(this);
        nameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    nameView.setError(null);
                    nameHasError = true;
                    return;
                }


                if (s.toString().matches(partialRegexChecks[2])) {
                    nameView.setError(getResources().getString(R.string.start_wnumber));
                    nameHasError = true;
                    return;
                }
                if (s.toString().matches(partialRegexChecks[3])) {
                    nameView.setError(getResources().getString(R.string.has_ilegal_character));
                    nameHasError = true;
                    return;
                }

                nameHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        lastNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    lastNameView.setError(null);
                    lastNameHasError = true;
                    return;
                }


                if (s.toString().matches(partialRegexChecks[2])) {
                    lastNameView.setError(getResources().getString(R.string.start_wnumber));
                    lastNameHasError = true;
                    return;
                }
                if (s.toString().matches(partialRegexChecks[3])) {
                    lastNameView.setError(getResources().getString(R.string.has_ilegal_character));
                    lastNameHasError = true;
                    return;
                }

                lastNameHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        maLastNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    maLastNameView.setError(null);
                    maLastNameHasError = false;
                    return;
                }


                if (s.toString().matches(partialRegexChecks[2])) {
                    maLastNameView.setError(getResources().getString(R.string.start_wnumber));
                    maLastNameHasError = true;
                    return;
                }
                if (s.toString().matches(partialRegexChecks[3])) {
                    maLastNameView.setError(getResources().getString(R.string.has_ilegal_character));
                    maLastNameHasError = true;
                    return;
                }

                maLastNameHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        rfcView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    rfcView.setError(null);
                    rfcHasError = true;
                    return;
                }


                if (!s.toString().matches(rfcRegex)) {
                    rfcView.setError(getResources().getString(R.string.ivalid_rfc));
                    rfcHasError = true;
                    return;
                }

                rfcHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(client != null)
            setupData();
    }

    private boolean checkViews(){
        nameView.setText(nameView.getText() != null ? nameView.getText() : "");
        lastNameView.setText(lastNameView.getText() != null ? lastNameView.getText() : "");
        maLastNameView.setText(maLastNameView.getText() != null ? maLastNameView.getText() : "");
        rfcView.setText(rfcView.getText() != null ? rfcView.getText() : "");

        if(nameView.length() == 0){
            nameView.setError(getResources().getString(R.string.empty_name));
        }

        if(lastNameView.length() == 0){
            lastNameView.setError(getResources().getString(R.string.empty_last_name));
        }

        if(rfcView.length() == 0){
            rfcView.setError(getResources().getString(R.string.empty_rfc));
        }


        return !nameHasError & !lastNameHasError & !maLastNameHasError & !rfcHasError;
    }

    private void saveClient(){
        if(checkViews()){
            progressDialog.show();
            JSONObject requestData = new JSONObject();

            try{

                requestData.put("name", nameView.getText());
                requestData.put("lastName", lastNameView.getText());
                requestData.put("maLastName", maLastNameView.getText());
                requestData.put("rfc", rfcView.getText());
                requestData.put("folio", folio);
            }catch (JSONException ex){
                ex.printStackTrace();
                return;
            }

            IAsyncApiRequest request = new AsyncApiRequest(getContext());

            if(isInEditMode){
                if(client != null){
                    Utils.CLIENT_ID = client.getId();
                    client = new Client(requestData);
                    client.setId(Utils.CLIENT_ID);
                }

                request.Put(TypeRequest.EDIT_CLIENT, requestData, new ApiResponse() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        try {
                            if (data.getBoolean("isValid")) {
                                onSaveGeoFenceSuccess();
                            } else {
                                onSaveGeoFenceFailed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSaveGeoFenceFailed();
                        }
                    }

                    @Override
                    public void onError(JSONObject error) {
                        onSaveGeoFenceFailed();
                    }

                    @Override
                    public void onRetry(int number) {

                    }

                    @Override
                    public void onProgress(int percent) {
                        if(progressDialog.isShowing())
                            progressDialog.setProgress(percent);
                    }

                    @Override
                    public void onConnection(boolean isConnected) {
                        if (!isConnected) {
                            onSaveGeoFenceFailed();
                        }
                    }
                });
            }else{

                client = new Client(requestData);
                request.Post(TypeRequest.SAVE_CLIENT, requestData, new ApiResponse() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        try {
                            if (data.getBoolean(Names.IS_VALID)) {
                                client.setId(data.getJSONObject(Names.DATA).getString("id"));
                                onSaveGeoFenceSuccess();
                            } else {
                                onSaveGeoFenceFailed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(JSONObject error) {
                        onSaveGeoFenceFailed();
                    }

                    @Override
                    public void onRetry(int number) {

                    }

                    @Override
                    public void onProgress(int percent) {

                    }

                    @Override
                    public void onConnection(boolean isConnected) {
                        if (!isConnected) {
                            Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }
                });
            }
        }
    }

    private void onSaveGeoFenceSuccess(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if(Utils.OBSERVABLE_CLIENT_DATA != null){
            Utils.OBSERVABLE_CLIENT_DATA.updateItem(client);
        }

        Toast.makeText(getContext(),getResources().getString(R.string.save_changes_ok), Toast.LENGTH_SHORT).show();
        clientIsSaved = true;
        dismiss();
    }

    private void onSaveGeoFenceFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.save_client_error), Toast.LENGTH_LONG).show();
        if(progressDialog.isShowing())
            progressDialog.dismiss();
        clientIsSaved = false;
    }


    private void setupData(){
        fragmentTitle.setText(getResources().getString(R.string.edit_client_title));
        nameView.setText(client.getName());
        lastNameView.setText(client.getLastName());
        maLastNameView.setText(client.getMaLastName());
        rfcView.setText(client.getRfc());
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.save_button){
            saveClient();
        }
    }
}

