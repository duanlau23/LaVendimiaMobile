package duan.test.lavendimia.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.DrawerFragmentListener;
import duan.test.lavendimia.utils.Utils;

public class DrawerFragment extends DialogFragment {

    View v;
    DrawerFragmentListener mListener;
    NavigationView navigationView;

    TextView mNameView;
    TextView mEmailView;
    CircleImageView profileImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);

        try {
            mListener = (DrawerFragmentListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnItemClickedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_drawer, container, false);

        navigationView = (NavigationView) v.findViewById(R.id.nav_view);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                mListener.onDrawerItemClick(item);
                return true;
            }
        });


        View headerView = LayoutInflater.from(getContext()).inflate(R.layout.nav_header, navigationView, false);
        navigationView.addHeaderView(headerView);
        mNameView = (TextView) headerView.findViewById(R.id.user_name);
        mEmailView = (TextView) headerView.findViewById(R.id.email);
        profileImage = (CircleImageView) headerView.findViewById(R.id.profile_image);

        mNameView.setText(Utils.USER_NAME != null ? Utils.USER_NAME : "");
        mEmailView.setText(Utils.EMAIL != null ? Utils.EMAIL : "");

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);


    }

    public void changeUserData(){
        mNameView.setText(Utils.USER_NAME != null ? Utils.USER_NAME : "");
        mEmailView.setText(Utils.EMAIL != null ? Utils.EMAIL : "");
    }
}
