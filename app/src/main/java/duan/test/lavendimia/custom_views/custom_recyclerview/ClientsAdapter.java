package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Client;

/**
 * Created by duan_ on 04/12/2016.
 */

public class ClientsAdapter extends RecyclerView.Adapter<ClientsViewHolder> {
    private List<Client> clientList;
    private ItemClickListener listener;

    public ClientsAdapter(List<Client> clientList, ItemClickListener listener){
        this.clientList = clientList;
        this.listener = listener;
    }

    @Override
    public ClientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.client_item, parent, false);
        return new ClientsViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final ClientsViewHolder holder, int position) {

        Client client = getItem(position);

        if(client == null){
            return;
        }

        holder.setupData(client);

    }


    @Override
    public int getItemCount() {
        return clientList.size();
    }

    private Client getItem(int i){
        try{
            return clientList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
