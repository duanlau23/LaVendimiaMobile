package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 04/12/2016.
 */

public class SaleItemAdapter extends RecyclerView.Adapter<SaleItemViewHolder> {
    private List<Item> itemList;
    private ItemClickListener listener;


    public SaleItemAdapter(List<Item> itemList, ItemClickListener listener){
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public SaleItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_item_item, parent, false);
        return new SaleItemViewHolder(view, listener, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final SaleItemViewHolder holder, int position) {

        Item item = getItem(position);
        if(item == null){
            return;
        }

        holder.setupData(item);

    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    private Item getItem(int i){
        try{
            return itemList.get(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
