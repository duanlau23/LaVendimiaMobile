package duan.test.lavendimia.listeners;

import java.util.List;

import duan.test.lavendimia.objects.Client;

public interface ObservableClientListener {

    void addItems(List<Client> items);

    void addItem(Client item);

    void removeItem(Client item);

}