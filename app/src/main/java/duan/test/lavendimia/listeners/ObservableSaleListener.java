package duan.test.lavendimia.listeners;

import java.util.List;

import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.objects.Sale;

public interface ObservableSaleListener {

    void addItems(List<Sale> items);

    void addItem(Sale item);

    void removeItem(Sale item);

}