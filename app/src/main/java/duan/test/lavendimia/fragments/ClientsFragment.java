package duan.test.lavendimia.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.custom_views.custom_recyclerview.ClientsAdapter;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.listeners.ObservableClientListener;
import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.observables.ClientObserver;
import duan.test.lavendimia.objects.observables.ObservableClient;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by duan_ on 25/02/2017.
 */

public class ClientsFragment extends Fragment implements
        ItemClickListener,
        ObservableClientListener{

    RecyclerView clientsRecyclerView;
    List<Client> clientsList;
    ClientsAdapter clientsAdapter;
    ProgressDialog progressDialog;
    EditClientFragment editClientFragment;
    ClientObserver clientObserver;
    View editClientView;
    View v;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);}

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.clients_fragment, container, false);
        setupViews();
        setupRecyclerView();
        getClients();
        return v;
    }

    private void setupViews(){
        clientsRecyclerView = (RecyclerView) v.findViewById(R.id.clients_list);
        editClientView = v.findViewById(R.id.edit_client);

        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.loading_clients));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
    }

    private void setupRecyclerView(){

        editClientView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editClient(null);
            }
        });

        if(Utils.OBSERVABLE_CLIENT_DATA == null) {
            clientsList = new ArrayList<>();
        }else{
            clientsList = new ArrayList<>();
            clientsList.addAll(Utils.OBSERVABLE_CLIENT_DATA.getData());
        }

        clientObserver = new ClientObserver(Utils.OBSERVABLE_CLIENT_DATA, this);
        Utils.OBSERVABLE_CLIENT_DATA = new ObservableClient(clientsList);
        Utils.OBSERVABLE_CLIENT_DATA.addObserver(clientObserver);

        clientsAdapter = new ClientsAdapter(clientsList, this );
        clientsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        clientsRecyclerView.setAdapter(clientsAdapter);
    }


    private void editClient(Client client){
        if (editClientFragment == null) {
            editClientFragment = new EditClientFragment();
        }

        if(editClientFragment.isVisible())
            return;

        if (editClientFragment.isAdded())
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();
        editClientFragment = new EditClientFragment();
        if(client != null)
        {
            Bundle data = new Bundle();
            data.putParcelable("sale", client);
            editClientFragment.setArguments(data);
        }

        editClientFragment.show(fm, "editItemFragment");
    }

    private void getClients(){
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        clientsList.clear();
        progressDialog.show();
        request.Get(TypeRequest.GET_ALL_CLIENTS, "", new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean(Names.IS_VALID)) {
                        onGetGeoFencesSuccess(data.getJSONArray("data"));
                    } else {
                        onGetGeoFencesFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    onGetGeoFencesFailed();
                }
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        });
    }

    private void onGetGeoFencesSuccess(JSONArray data){
        try{
            Client client;
            for (int x = 0; x < data.length(); x++){
                client = new Client(data.getJSONObject(x));
                clientsList.add(client);
            }
            dismissDialog();
            clientsAdapter.notifyDataSetChanged();
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    private void onGetGeoFencesFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.get_clients_error), Toast.LENGTH_LONG).show();
        dismissDialog();
    }

    private void dismissDialog(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onItemClick(Object item) {
        editClient((Client) item);
    }

    @Override
    public void onEditItemClick(Object item) {
        editClient((Client) item);
    }

    @Override
    public void addItems(List<Client> items) {
        clientsAdapter.notifyDataSetChanged();
    }

    @Override
    public void addItem(Client data) {
        clientsAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeItem(Client item) {
        clientsAdapter.notifyDataSetChanged();
    }
}
