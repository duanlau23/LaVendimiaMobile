/**
 * Created by duan_ on 26/02/2017.
 */
/**
 * Created by ben on 09/11/2015.
 */
module.exports =  function(mongoose,connection){
    var clientsSchema  = {
        folio: {type:String, index: {unique:true}, required:true},
        name: {type:String, index: {unique:false}, required:true},
        lastName: {type: String, index: {unique:false}, required:true},
        maLastName: {type:String, index: {unique:false}, required:false},
        rfc: {type:String, index: {unique:false}, required:true}
    };

    return {
        model : connection.model('clients',clientsSchema),
        schema : {
            folio: "required",
            name: "required",
            lastName: "required",
            rfc: "required"
        }
    }
};