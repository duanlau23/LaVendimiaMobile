package duan.test.lavendimia.objects.observables;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import duan.test.lavendimia.objects.Item;

/**
 * Created by duan_ on 26/02/2017.
 */

public class ObservableItem extends Observable {

    private List<Item> data = new ArrayList<>();

    public ObservableItem(List<Item> data)
    {
        this.data = data;
    }


    public void setValue(List<Item> data)
    {
        this.data.addAll(data);
        setChanged();
        notifyObservers();
    }

    void addItem(Item item){
        this.data.add(item);
        setChanged();
        notifyObservers(item);
    }

    public void updateItem(Item item){
        boolean isFound = false;
        for (int x = 0;x<this.data.size();x++){
            if(this.data.get(x).getId().equals(item.getId())) {
                this.data.set(x, item);
                isFound = true;
                break;
            }
        }
        if(isFound) {
            setChanged();
            notifyObservers();
        }else{
            addItem(item);
        }
    }

    public void update(){
        setChanged();
        notifyObservers();
    }

    public void removeItem(String devideId){
        for (Item reportD:this.data) {
            if(reportD.getId().equals(devideId)) {
                this.data.remove(reportD);
                break;
            }
        }
        setChanged();
        notifyObservers();
    }

    public List<Item> getData(){
        return this.data;
    }

    public void removeItems(List<Item> data){
        this.data.removeAll(data);
        setChanged();
        notifyObservers();
    }
    public List<Item>  getValue()
    {
        return data;
    }

}