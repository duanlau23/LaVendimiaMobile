package duan.test.lavendimia.objects;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import duan.test.lavendimia.utils.Names;

/**
 * Created by duan_ on 25/02/2017.
 */

public class Client implements Parcelable, Serializable {

    private String id;
    private String name;
    private String lastName;
    private String maLastName;
    private String rfc;
    private String folio;

    public Client(){}

    public Client(JSONObject data){
        try{
            this.id = data.has(Names.ID) ? data.getString(Names.ID):"";
            this.name = data.getString(Names.NAME);
            this.lastName = data.getString(Names.LAST_NAME);
            this.maLastName = data.has(Names.MA_LAST_NAME) ? data.getString(Names.MA_LAST_NAME) : "";
            this.rfc = data.getString(Names.RFC);
            this.folio = data.getString(Names.FOLIO);
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaLastName() {
        return maLastName;
    }

    public void setMaLastName(String maLastName) {
        this.maLastName = maLastName;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.lastName);
        dest.writeString(this.maLastName);
        dest.writeString(this.rfc);
        dest.writeString(this.folio);
    }

    protected Client(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.lastName = in.readString();
        this.maLastName = in.readString();
        this.rfc = in.readString();
        this.folio = in.readString();
    }

    public static final Parcelable.Creator<Client> CREATOR = new Parcelable.Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel source) {
            return new Client(source);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };
}
