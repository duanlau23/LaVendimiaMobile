package duan.test.lavendimia.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;

import duan.test.lavendimia.R;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.objects.Item;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;


public class EditItemFragment extends DialogFragment
        implements View.OnClickListener {

    View v;
    Item item;
    EditText descriptionView;
    EditText modelView;
    EditText priceView;
    EditText existenceView;
    TextView fragmentTitle;
    TextView folioView;
    View saveButton;
    boolean descriptionHasError;
    boolean modelHasError;
    boolean priceHasError;
    boolean existenceError;
    private boolean isInEditMode;
    private boolean itemIsSaved;
    ProgressDialog progressDialog;
    String folio;
    String moneyRegex = "[0-9]+([,.][0-9]{1,2})?";
    String[] partialRegexChecks = {
            ".*[a-z]+.*", // lower
            ".*[A-Z]+.*", // upper
            ".*[0-9]+.*", // digits
            ".*[@#$%+\\-;\\*<>=.,]+.*" // symbols
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() == null)
            return;

        item = getArguments().getParcelable("sale");
        isInEditMode = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar actionBar = (Toolbar) v.findViewById(R.id.settings_toolbar);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
        if (actionBar != null) {
            final EditItemFragment window = this;
            actionBar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                }
            });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createDialog();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

    }

    public AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppThemeDark);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                if(itemIsSaved)
                    return true;

                if ((i ==  KeyEvent.KEYCODE_BACK) && keyEvent.getAction() == KeyEvent.ACTION_DOWN)
                {
                    new AlertDialog.Builder(getContext(), R.style.AppTheme_Dark_Dialog)
                            .setIcon(R.drawable.ic_vn_alert)
                            .setTitle(R.string.client_alert_title)
                            .setMessage(R.string.client_alert_description)
                            .setPositiveButton(R.string.yes_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dismiss();
                                }

                            })
                            .setNegativeButton(R.string.no_text, null)
                            .show();
                    return false;
                }
                else
                    return false;
            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_edit_item, null);
        setupViews();
        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        descriptionView = (EditText) v.findViewById(R.id.description_view);
        modelView = (EditText) v.findViewById(R.id.model_view);
        priceView = (EditText) v.findViewById(R.id.price_view);
        existenceView = (EditText) v.findViewById(R.id.existence_view);
        fragmentTitle = (TextView) v.findViewById(R.id.fragment_title);
        saveButton = v.findViewById(R.id.save_button);
        folioView = (TextView) v.findViewById(R.id.folio_view);

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.saving_title_text));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);

        if(item != null){
            folio = item.getFolio();
        }else{
            folio = "0001";
            if(Utils.OBSERVABLE_ITEM_DATA != null){
                if(Utils.OBSERVABLE_ITEM_DATA.getData().size() < 9999){
                    folio = folio.substring(0, (folio.length())-String.valueOf(Utils.OBSERVABLE_ITEM_DATA.getData().size()).length()) + String.valueOf(Utils.OBSERVABLE_ITEM_DATA.getData().size()+1);
                }
            }
        }



        folioView.setText(folio);

        saveButton.setOnClickListener(this);
        descriptionView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    descriptionView.setError(null);
                    descriptionHasError = true;
                    return;
                }

                if (s.toString().matches(partialRegexChecks[3])) {
                    descriptionView.setError(getResources().getString(R.string.has_ilegal_character));
                    descriptionHasError = true;
                    return;
                }

                descriptionHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        modelView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().matches(partialRegexChecks[3])) {
                    modelView.setError(getResources().getString(R.string.has_ilegal_character));
                    modelHasError = true;
                    return;
                }

                modelHasError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        priceView.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    priceView.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100));

                    current = formatted;
                    priceView.setText(formatted);
                    priceView.setSelection(formatted.length());

                    priceView.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        existenceView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    existenceView.setError(null);
                    existenceError = true;
                    return;
                }

                existenceError = false;

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if(item != null)
            setupData();
    }

    private boolean checkViews(){
        descriptionView.setText(descriptionView.getText() != null ? descriptionView.getText() : "");
        priceView.setText(priceView.getText() != null ? priceView.getText() : "");
        existenceView.setText(existenceView.getText() != null ? existenceView.getText() : "");

        if(descriptionView.length() == 0){
            descriptionView.setError(getResources().getString(R.string.empty_description));
            descriptionHasError = true;
        }

        if(priceView.length() == 0){
            priceView.setError(getResources().getString(R.string.empty_price));
            priceHasError = true;
        }else{
            priceHasError = false;
        }

        if(existenceView.length() == 0){
            existenceView.setError(getResources().getString(R.string.empty_existence));
            existenceError = true;
        }

        return !descriptionHasError & !priceHasError & !modelHasError & !existenceError;
    }

    private void saveItem(){
        if(checkViews()){
            progressDialog.show();
            JSONObject requestData = new JSONObject();

            try{

                requestData.put("description", descriptionView.getText());
                requestData.put("model", modelView.getText());
                requestData.put("price",(NumberFormat.getCurrencyInstance().format((Double.parseDouble(priceView.getText().toString().
                        replaceAll("[$,.]", "")))/100)).replaceAll("[$,]", ""));
                requestData.put("existence", Integer.valueOf(existenceView.getText().toString()));
                requestData.put("folio", folio);
            }catch (JSONException ex){
                ex.printStackTrace();
                return;
            }

            IAsyncApiRequest request = new AsyncApiRequest(getContext());

            if(isInEditMode){
                if(item != null){
                    Utils.ITEM_ID = item.getId();
                    item = new Item(requestData);
                    item.setId(Utils.ITEM_ID);
                }

                request.Put(TypeRequest.EDIT_ITEM, requestData, new ApiResponse() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        try {
                            if (data.getBoolean("isValid")) {
                                onSaveGeoFenceSuccess();
                            } else {
                                onSaveGeoFenceFailed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSaveGeoFenceFailed();
                        }
                    }

                    @Override
                    public void onError(JSONObject error) {
                        onSaveGeoFenceFailed();
                    }

                    @Override
                    public void onRetry(int number) {

                    }

                    @Override
                    public void onProgress(int percent) {
                        if(progressDialog.isShowing())
                            progressDialog.setProgress(percent);
                    }

                    @Override
                    public void onConnection(boolean isConnected) {
                        if (!isConnected) {
                            onSaveGeoFenceFailed();
                        }
                    }
                });
            }else{

                item = new Item(requestData);
                request.Post(TypeRequest.SAVE_ITEM, requestData, new ApiResponse() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        try {
                            if (data.getBoolean(Names.IS_VALID)) {
                                item.setId(data.getJSONObject(Names.DATA).getString("id"));
                                onSaveGeoFenceSuccess();
                            } else {
                                onSaveGeoFenceFailed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(JSONObject error) {
                        onSaveGeoFenceFailed();
                    }

                    @Override
                    public void onRetry(int number) {

                    }

                    @Override
                    public void onProgress(int percent) {

                    }

                    @Override
                    public void onConnection(boolean isConnected) {
                        if (!isConnected) {
                            Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }
                });
            }
        }
    }

    private void onSaveGeoFenceSuccess(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        if(Utils.OBSERVABLE_ITEM_DATA != null){
            Utils.OBSERVABLE_ITEM_DATA.updateItem(item);
        }

        Toast.makeText(getContext(),getResources().getString(R.string.save_changes_ok), Toast.LENGTH_SHORT).show();

        itemIsSaved = true;
        dismiss();
    }

    private void onSaveGeoFenceFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.save_client_error), Toast.LENGTH_LONG).show();
        if(progressDialog.isShowing())
            progressDialog.dismiss();
        itemIsSaved = false;
    }


    private void setupData(){
        fragmentTitle.setText(getResources().getString(R.string.edit_item_title));
        descriptionView.setText(item.getDescription());
        modelView.setText(item.getModel());
        priceView.setText(item.getPrice());
        existenceView.setText(String.valueOf(item.getExistence()));
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.save_button){
            saveItem();
        }
    }
}

