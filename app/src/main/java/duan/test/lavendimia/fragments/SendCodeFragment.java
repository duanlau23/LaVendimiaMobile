package duan.test.lavendimia.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import duan.test.lavendimia.R;
import duan.test.lavendimia.activities.MainActivity;
import duan.test.lavendimia.db.DbHelper;
import duan.test.lavendimia.enums.FragmentAction;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.FragmentActionListener;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Utils;

public class SendCodeFragment  extends BaseDialogFragment {
    private static final Pattern codePattern
            = Pattern.compile("^([1-9][0-9]{0,2})?(\\.[0-9]?)?$");
    private ProgressDialog progressDialog;
    private EditText mCodeTextView;
    private Button mSendCode;
    private View v;
    private boolean mCodeHasError = true;
    FragmentActionListener mListener;

    public SendCodeFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createLoginDialogo();
    }

    @Override
    public void onStart() {
        super.onStart();// safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.slide_animation);

        try {

            mListener = (FragmentActionListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnItemClickedListener");
        }
        // alternative way of doing it
        //getDialog().getWindow().getAttributes().
        //    windowAnimations = R.style.dialog_animation_fade;

    }

    /**
     * Crea un diÃ¡logo con personalizado para comportarse
     * como formulario de login
     *
     * @return DiÃ¡logo
     */
    public AlertDialog createLoginDialogo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.slide_animation);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        v = inflater.inflate(R.layout.fragment_send_code, null);

        setupViews();

        builder.setView(v);
        return builder.create();
    }


    private void setupViews() {
        mCodeTextView = (EditText) v.findViewById(R.id.verification_code);
        mSendCode = (Button) v.findViewById(R.id.send_code_button);

        mSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkViews())
                    sendCode();
            }
        });

        mCodeTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mCodeTextView.setError(null);
                    mCodeHasError = true;
                }


                if (s.length() < getResources().getInteger(R.integer.validation_code_max_length)) {
                    mCodeTextView.setError(getResources().getString(R.string.invalid_code_length_error));
                    mCodeHasError = true;
                }

                if (codePattern.matcher(s).matches()) {
                    mCodeTextView.setError(getResources().getString(R.string.invalid_code));
                    mCodeHasError = true;
                }

                mCodeHasError = false;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private boolean checkViews() {
        if (mCodeTextView.getText().length() == 0) {
            mCodeTextView.setError(getResources().getString(R.string.empty_code));
            mCodeHasError = true;
        }


        if (mCodeTextView.getText().length() < getResources().getInteger(R.integer.validation_code_max_length) &&
                mCodeTextView.length() > 0) {
            mCodeTextView.setError(getResources().getString(R.string.invalid_code_length_error));
            mCodeHasError = true;
        }
        return mCodeHasError;
    }

    private void sendCode() {


        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getResources().getString(R.string.sending_code_text));
        progressDialog.show();


        JSONObject data = new JSONObject();
        try {
            data.put("code", mCodeTextView.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        onSendCodeSuccess(data);
    }

    public void onSendCodeSuccess(JSONObject data) {
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        request.Post(TypeRequest.SEND_CODE, data, new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean("isValid")) {
                        DbHelper helper = DbHelper.getInstance(getContext());
                        helper.saveToken(data);
                        checkStatus();
                    } else {
                        onSendCodeFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(JSONObject error) {
                onSendCodeFailed();
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), TypeRequest.SEND_CODE.getErrorText(getResources()), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void checkStatus() {
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        request.Post(TypeRequest.CHECK_STATUS, new JSONObject(), new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (!data.getBoolean("isValid")) {
                        onSendCodeFailed();
                        mCodeTextView.setError(getResources().getString(R.string.invalid_code));
                    } else {
                        DbHelper helper = DbHelper.getInstance(getContext());
                        helper.saveUserData(data);
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();

                        mListener.onFragmentAction(FragmentAction.SIGN_UP_SUCCESS);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    onSendCodeFailed();
                }
            }

            @Override
            public void onRetry(int number) {
                onSendCodeFailed();
            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (isConnected) {
                    DbHelper helper = DbHelper.getInstance(getContext());
                    helper.refreshUserData();
                    if (Utils.USER_ID != null) {
                        onSendCodeFailed();
                    }
                }
            }
        });
    }

    public void onSendCodeFailed() {
        progressDialog.dismiss();

    }
}
