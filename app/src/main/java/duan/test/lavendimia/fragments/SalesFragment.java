package duan.test.lavendimia.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import duan.test.lavendimia.R;
import duan.test.lavendimia.custom_views.custom_recyclerview.SaleAdapter;
import duan.test.lavendimia.enums.TypeRequest;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.listeners.ObservableSaleListener;
import duan.test.lavendimia.objects.Sale;
import duan.test.lavendimia.objects.observables.ObservableSale;
import duan.test.lavendimia.objects.observables.SaleObserver;
import duan.test.lavendimia.task.AsyncApiRequest;
import duan.test.lavendimia.task.IAsyncApiRequest;
import duan.test.lavendimia.utils.ApiResponse;
import duan.test.lavendimia.utils.Names;
import duan.test.lavendimia.utils.Utils;

/**
 * Created by duan_ on 25/02/2017.
 */

public class SalesFragment extends Fragment implements
        ItemClickListener,
        ObservableSaleListener{

    RecyclerView saleRecyclerView;
    List<Sale> saleList;
    SaleAdapter saleAdapter;
    ProgressDialog progressDialog;
    AddSaleFragment addSaleFragment;
    SaleObserver saleObserver;
    View addSale;
    View v;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);}

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.sales_fragment, container, false);
        setupViews();
        setupRecyclerView();
        getSale();
        return v;
    }

    private void setupViews(){
        saleRecyclerView = (RecyclerView) v.findViewById(R.id.sales_list);
        addSale = v.findViewById(R.id.add_sale);

        progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(getResources().getString(R.string.loading_sales));
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
    }

    private void setupRecyclerView(){

        addSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSale(null);
            }
        });

        if(Utils.OBSERVABLE_SALE_DATA == null) {
            saleList = new ArrayList<>();
        }else{
            saleList = new ArrayList<>();
            saleList.addAll(Utils.OBSERVABLE_SALE_DATA.getData());
        }

        saleObserver = new SaleObserver(Utils.OBSERVABLE_SALE_DATA, this);
        Utils.OBSERVABLE_SALE_DATA = new ObservableSale(saleList);
        Utils.OBSERVABLE_SALE_DATA.addObserver(saleObserver);

        saleAdapter = new SaleAdapter(saleList, this );
        saleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        saleRecyclerView.setAdapter(saleAdapter);
    }


    private void addSale(Sale sale){
        if (addSaleFragment == null) {
            addSaleFragment = new AddSaleFragment();
        }

        if(addSaleFragment.isVisible())
            return;

        if (addSaleFragment.isAdded())
            return;

        FragmentManager fm = getActivity().getSupportFragmentManager();
        addSaleFragment = new AddSaleFragment();
        if(sale != null)
        {
            Bundle data = new Bundle();
            data.putParcelable("sale", sale);
            addSaleFragment.setArguments(data);
        }

        addSaleFragment.show(fm, "editItemFragment");
    }

    private void getSale(){
        IAsyncApiRequest request = new AsyncApiRequest(getContext());
        saleList.clear();
        progressDialog.show();
        request.Get(TypeRequest.GET_ALL_SALES, "", new ApiResponse() {
            @Override
            public void onSuccess(JSONObject data) {
                try {
                    if (data.getBoolean(Names.IS_VALID)) {
                        onGetGeoFencesSuccess(data.getJSONObject(Names.DATA).getJSONArray("items"));
                    } else {
                        onGetGeoFencesFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(JSONObject error) {
                if (error != null) {
                    onGetGeoFencesFailed();
                }
            }

            @Override
            public void onRetry(int number) {

            }

            @Override
            public void onProgress(int percent) {

            }

            @Override
            public void onConnection(boolean isConnected) {
                if (!isConnected) {
                    Toast.makeText(getContext(), getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        });
    }

    private void onGetGeoFencesSuccess(JSONArray data){
        try{
            Sale sale;
            for (int x = 0; x < data.length(); x++){
                sale = new Sale(data.getJSONObject(x));
                saleList.add(sale);
            }
            dismissDialog();
            saleAdapter.notifyDataSetChanged();
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    private void onGetGeoFencesFailed(){
        Toast.makeText(getContext(), getResources().getString(R.string.get_sales_error), Toast.LENGTH_LONG).show();
        dismissDialog();
    }

    private void dismissDialog(){
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onItemClick(Object item) {
        addSale((Sale) item);
    }

    @Override
    public void onEditItemClick(Object item) {
        addSale((Sale) item);
    }

    @Override
    public void addItems(List<Sale> items) {
        saleAdapter.notifyDataSetChanged();
    }

    @Override
    public void addItem(Sale data) {
        saleAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeItem(Sale item) {
        saleAdapter.notifyDataSetChanged();
    }


}
