package duan.test.lavendimia.listeners;

import duan.test.lavendimia.enums.SettingType;

/**
 * Created by duan_ on 28/01/2017.
 */

public interface SettingsChangeListener {

    void onSettingChange(SettingType settingType);

}
