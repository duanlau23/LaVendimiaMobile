package duan.test.lavendimia.listeners;

public interface CheckStatusListener {

    void onCheckStatusSuccess();

    void onCheckStatusFailed();

    void onCheckStatusRepeat();

}
