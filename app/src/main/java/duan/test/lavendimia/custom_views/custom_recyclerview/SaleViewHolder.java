package duan.test.lavendimia.custom_views.custom_recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.text.NumberFormat;
import java.util.Locale;

import duan.test.lavendimia.R;
import duan.test.lavendimia.listeners.ItemClickListener;
import duan.test.lavendimia.objects.Sale;

/**
 * Created by duan_ on 04/12/2016.
 */
class SaleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView clientName;
    private TextView clientFolio;
    private TextView totalView;
    private TextView dateView;
    private TextView saleFolio;
    private ItemClickListener listener;
    private Sale sale;

    SaleViewHolder(View view, ItemClickListener listener) {
        super(view);
        clientName = (TextView) view.findViewById(R.id.client_name);
        clientFolio = (TextView) view.findViewById(R.id.client_folio);
        totalView = (TextView) view.findViewById(R.id.total_view);
        dateView = (TextView) view.findViewById(R.id.date_view);
        saleFolio = (TextView) view.findViewById(R.id.sale_folio);
        this.listener = listener;
    }

    void setupData(Sale sale){
        this.sale = sale;
        clientName.setText(sale.getClient().getName());
        clientFolio.setText(sale.getClient().getFolio());
        totalView.setText(String.valueOf(NumberFormat.getCurrencyInstance().format(sale.getTotal())));
        dateView.setText(sale.getDate().toString("dd/MM/yyyy", Locale.getDefault()));
        saleFolio.setText(sale.getFolio());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_client:
                listener.onEditItemClick(sale);
        }
    }
}
