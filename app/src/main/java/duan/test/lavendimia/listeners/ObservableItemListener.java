package duan.test.lavendimia.listeners;

import java.util.List;

import duan.test.lavendimia.objects.Client;
import duan.test.lavendimia.objects.Item;

public interface ObservableItemListener {

    void addItems(List<Item> items);

    void addItem(Item item);

    void removeItem(Item item);

}