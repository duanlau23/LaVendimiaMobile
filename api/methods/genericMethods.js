// Método Post Genérico
app.post('/:model',  utils.checkParams, function(req, res) {
    try {
        if (Object.keys(pModels).indexOf(req.params.model) === -1){
            res.json(resultUtils.toResult(false, resultType.error, null , null, {code:'CM-0005'}));
        }else{
            var model = pModels[req.params.model];
            var newData = utils.fillParams(req.body.value, model.schema, true);
            console.log(newData,":::::::::::::::::::::::: POST :::::::::::::::::::::::");
            if (newData.isValid) {
                var newModel = new model.model(
                    newData.data
                );

                var doSave = {isValid : true};
                if (typeof model.beforePost === 'function'){
                    doSave = model.beforePost(newModel);
                }
                if (doSave.isValid === true) {
                    newModel.save(function (err) {
                        if (err) {
                            res.json(resultUtils.toResult(false, resultType.error, null, null, err));
                        } else {
                            var msg = null;
                            if (typeof model.afterPost === 'function'){
                                msg = model.afterPost(newModel);
                            }

                            res.json(resultUtils.toResult(true, resultType.success, {id: newModel._id}, msg));
                        }
                    });
                }else{
                    res.json(resultUtils.toResult(false, resultType.info, null, null, {code:'CM-0002', payload: doSave.payload}));
                }

            }else{
                res.json(resultUtils.toResult(false, resultType.error, null, newData.message, newData.message));
            }
        }
    }catch(innerEx){
        res.json(resultUtils.toResult(false, resultType.error, null , innerEx.toString(), innerEx));
    }
});

// Método Put Genérico
app.put('/:model/:id', utils.checkParams, function(req, res) {
    try {
        var model = pModels[req.params.model];
        var newData = utils.fillParams(req.body.value, model.schema, false).data;
        var id = req.params.id;
        model.model.findOneAndUpdate({"_id":id}, newData, function (err, doc) {
            if (err){
                res.json(resultUtils.toResult(false,resultType.error,null,null, err));
            }else if(doc){
                var olddoc = {};
                Object.keys(newData).forEach(function(key){
                    if (model.schema[key]) {
                        if (model.schema[key].indexOf('private') === -1) {
                            olddoc[key] = doc[key];
                        }
                    }else{
                        olddoc[key] = doc[key];
                    }
                });
                var msg = null;
                if (typeof model.afterPut === 'function'){
                    msg = model.afterPut({ old:olddoc , new:newData });
                }
                res.json(resultUtils.toResult(true,resultType.success,{ old:olddoc , new:newData },msg ));
            }else{
                res.json(resultUtils.toResult(false,resultType.error,null,"Id not found", {errmsg:'ID not Found'}));
                //res.json(resultUtils.toResult(false,resultType.error,null,"Id not found", {message:'ID not Found'}));
            }
        });
    }catch(innerEx){
        res.json(resultUtils.toResult(false, resultType.error, null , innerEx.toString(), innerEx));
    }
});

// Método Get genérico
app.get('/:model',tokenManager.middleware, function(req, res) {
    try {
        var model =  pModels[req.params.model];
        var query = {};
        if (req.query.q) {
            query = JSON.parse(req.query.q);
        }

        if (req.query.rexp){
            if (req.query.rexpfield){
                query[req.query.rexpfield] = new RegExp(req.query.rexp, "i");
            }
        }

        var limit = 30;
        var skip = 0;
        var sort = {};

        if (req.query.limit) {
            limit = parseInt(req.query.limit);
            if (limit > 50){
                limit = 50;
            }
        }
        if (req.query.skip) {
            skip = parseInt(req.query.skip);
        }

        if (req.query.sort) {
            sort= req.query.sort;
        }

        var privateFields = "";

        if (req.query.fields) {
            var fields = req.query.fields.split(',');
            fields.forEach(function (field) {
                try {
                    if (Object.keys(model.schema)[field].indexOf('private') == -1) {
                        privateFields += field + " ";
                    }
                } catch (ex) {
                    privateFields += field + " ";
                }
            });
        }else{
            Object.keys(model.schema).forEach(function(key){
                if (model.schema[key].indexOf('private') > -1){
                    privateFields += "-" + key + " ";

                }
            });
        }

        var isLogged = false;

        //todo: Validar token que viene desde la cookie o desde los headers
        if (req.cookies.dtcoo && !req.headers[config.headers.sessionHeader])
        {
            req.headers[config.headers.sessionHeader] = req.cookies.dtcoo;
        }
        /* if (req.cookies.dtcoo && !req.headers[config.headers.sessionHeaderCallCenter])
         {
         req.headers[config.headers.sessionHeaderCallCenter] = req.cookies.dtcoo;
         }
         */


         var next = function(){
            if (typeof model.beforeGet === 'function'){
                query = model.beforeGet(query,isLogged);
            }
            if (req.query._u){
                query['userId'] = req.query._u;
            }
            if (req.params.model.toLowerCase() === 'profiles'){
                if (sort)
                    sort += ' -votesCount -logo';
                else{
                    sort = '-votesCount -logo';
                }
                console.log(sort);
            }
            model.model.find(query, privateFields).skip(skip).limit(limit).sort(sort).exec(function (err, docs) {
                if (err) {
                    console.log(err);
                    res.send(resultUtils.toResult(false, resultType.error, null, null, err));
                } else {
                    if (typeof model.afterGet === 'function') {
                        docs = model.afterGet(docs, isLogged);
                    }
                    res.send(resultUtils.toResult(true, resultType.success, docs));
                }
            });
        };
        var userToken = req.headers[config.headers.sessionHeader];
        //  var userToken = req.headers[config.headers.sessionHeaderCallCenter];
        var remember = Boolean(req.headers[config.headers.sessionRemember]);
        if (typeof userToken !== 'undefined' &&
            userToken !== '' &&
            userToken!== null) {

            var sessionModel = db.model("sessions");
            sessionModel.findOne({token : userToken},function(err,doc) {
                if (doc) {
                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    var now = new Date();
                    if (doc.valid_at < now){
                        isLogged = false;
                        next();
                    }else if (doc.tokenBody.ip != ip){
                        isLogged = false;
                        next();
                    }else if (doc.tokenBody.appId != req.headers[config.headers.appHeader]){
                        isLogged = false;
                        next();
                    }else{
                        if (remember === true)
                            var valid_at = new Date(now.getTime() + (1000 * (60 * 60 * 24 * 30)));
                        else
                            var valid_at = new Date(now.getTime() + (1000 * (60 * 30)));
                        var newValid = { valid_at :  valid_at};
                        sessionModel.update(doc,newValid, function (err, doc) {
                            if (err){
                                isLogged = false;
                            }else isLogged = !!doc;
                            next();
                        });
                    }
                } else {
                    isLogged = false;
                    next();
                }
            });
        } else {
            isLogged = false;
            next();
        }
    }catch(innerEx){
        res.send(resultUtils.toResult(false, resultType.error, null , innerEx.toString(), innerEx));
    }
});
